//
//  ResidentIdOptionViewController.swift
//  AlzhaTV
//
//  Created by QMCPL on 02/04/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit
import Parse
import MobileCoreServices
import CoreFoundation
import MediaPlayer
import AVFoundation

class ResidentIdOptionViewController: UIViewController, UIAlertViewDelegate
{
    @IBOutlet var btnLogout: UIButton!
    @IBOutlet var btnFirstTimeSetup: UIButton!
    @IBOutlet var btnAlreadyResidentId: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addUIComponents()
    }
    
    func addUIComponents()
    {
        self.btnFirstTimeSetup.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)
        self.btnFirstTimeSetup.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnFirstTimeSetup.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
        self.btnFirstTimeSetup.layer.cornerRadius = 10.0
        
        self.btnAlreadyResidentId.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)
        self.btnAlreadyResidentId.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnAlreadyResidentId.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
        self.btnAlreadyResidentId.layer.cornerRadius = 10.0
        
        self.btnLogout.backgroundColor = UIColor.redColor()
        self.btnLogout.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnLogout.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
        self.btnLogout.layer.cornerRadius = 10.0
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBar.hidden = true
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnFirstTimeSetupClicked(sender: AnyObject)
    {
        let storyboard:UIStoryboard = self.storyboard!
        let view:ResidentIdViewController = storyboard.instantiateViewControllerWithIdentifier("ResidentIdViewController") as! ResidentIdViewController
        view.isFirstTimeSetup = true
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    @IBAction func btnAlreadyResidentIdClicked(sender: AnyObject)
    {
        let storyboard:UIStoryboard = self.storyboard!
        let view:ResidentIdViewController = storyboard.instantiateViewControllerWithIdentifier("ResidentIdViewController") as! ResidentIdViewController
        view.isFirstTimeSetup = false
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    //MARK:- Logout Button Action
    @IBAction func btnLogoutClicked(sender: AnyObject)
    {
        //-----
        if (objc_getClass("UIAlertController") != nil)
        {
            if #available(iOS 8.0, *) {
                let alert: UIAlertController = UIAlertController(title: "Logout?", message: "Do you want to logout?", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler:{ action in
                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
                    })
                }))
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{ action in
                    PFUser.logOut()
                    UserDefault.setUsersResidentId("")
                    self.navigationController?.popToRootViewControllerAnimated(true)
                    self.dismissViewControllerAnimated(true, completion:{ () -> Void in
                    })
                }))
                self.presentViewController(alert, animated: true, completion: { () -> Void in
                })
            } else {
                // Fallback on earlier versions
            }
            
        }
        else
        {
            let alert = UIAlertView(title: "Logout?", message: "Do you want to logout?", delegate: self , cancelButtonTitle:"No", otherButtonTitles:"Yes")
            alert.show()
        }
        //-----
    }
    
    //MARK:- UIAlertViewDelegate Method
    func alertView(View: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex
        {
        case 0:
            print("0 case")
            
        case 1:
            PFUser.logOut()
            UserDefault.setUsersResidentId("")
            self.navigationController?.popToRootViewControllerAnimated(true)
            
        default:
            print("Default case")
        }
    }
}
