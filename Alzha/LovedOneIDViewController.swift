//
//  LovedOneIDViewController.swift
//  AlzhaTV
//
//  Created by Ashwini on 22/06/15.
//  Copyright (c) 2015 QMC243-MAC-ShrikantP. All rights reserved.
//

import UIKit
import Parse

class LovedOneIDViewController: UIViewController,UITextFieldDelegate{
    
    @IBOutlet var textFieldName: UITextField!
    @IBOutlet var textFieldLovedOneID: UITextField!
    @IBOutlet var btnContinue: UIButton!
    var activityIndicator:UIActivityIndicatorView!
    var user:PFUser!
    var isInvitedUser : Bool = false;
    var lovedOneId : String = "";
    var lovedOneName : String = "";
    @IBOutlet weak var labelMessage: UILabel!
    //MARK: INITIAL SETUP
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.user = PFUser.currentUser()
        
        self.addUiSetup()
        self.addTapGesture()
        
        
        self.getLovedOneIdFromWeb();
        
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBar.hidden = true
        self.navigationController?.navigationItem.hidesBackButton = true
    }
    
    func addUiSetup()
    {
        self.btnContinue.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)
        self.btnContinue.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnContinue.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
        self.btnContinue.layer.cornerRadius = 10.0
        
        self.textFieldName.layer.borderWidth = 0.5
        self.textFieldName.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.textFieldName.textColor = UIColor.darkGrayColor()
        self.textFieldName.delegate = self
        self.textFieldName.tag = 0;
        self.textFieldName.returnKeyType = UIReturnKeyType.Done
        
        self.textFieldLovedOneID.layer.borderWidth = 0.5
        self.textFieldLovedOneID.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.textFieldLovedOneID.textColor = UIColor.darkGrayColor()
        self.textFieldLovedOneID.delegate = self
        lovedOneId = self.generateID() as String
        self.textFieldLovedOneID.placeholder = "Loved one ID : \(lovedOneId)"
        self.textFieldLovedOneID.tag = 1;
        self.textFieldLovedOneID.returnKeyType = UIReturnKeyType.Done
        
        
    }
    
    func addTapGesture()
    {
        let tapSelector : Selector = "endEditing";
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: tapSelector);
        self.view.addGestureRecognizer(tap);
    }
    
    //MARK: LOGICAL METHODS & ACTIONS
    func generateID() -> NSMutableString
    {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: 6)
        
        for (var i=0; i < 6; i++){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        
        return randomString
        
        
    }
    
    
    func endEditing()
    {
        self.view.endEditing(true);
    }
    
    @IBAction func btnContinueClicked(sender: AnyObject) {
        let result = self.checkFieldsComplete()
        
        if(result.isEmpty)
        {
            self.registerLovedOneID()
        }
        else
        {
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Alert", message: result, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
               
            }
            else
            {
                let alert = UIAlertView(title: "Alert", message: result, delegate: nil , cancelButtonTitle:"Ok")
                alert.show()
            }
        }
        
    }
    func checkFieldsComplete() ->String
    {
        
        
        if(self.textFieldLovedOneID.text!.isEmpty  || self.textFieldName.text!.isEmpty )
        {
            return "Enter your Loved One Name & Id."
        }
        else
        {
            return ""
        }
    }
    
    
    
    //MARK: UI Logics
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
        self.view.userInteractionEnabled = false;
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
        self.view.userInteractionEnabled = true
    }
    
    func pushRecordView()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let storyboard:UIStoryboard = self.storyboard!
        
        let menuViewController:MenuViewController = storyboard.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
        
        let view:RecordVideoViewController = storyboard.instantiateViewControllerWithIdentifier("RecordVideoViewController") as! RecordVideoViewController
        
        let revealController:SWRevealViewController  = SWRevealViewController()
        revealController.rearViewController = menuViewController
        revealController.frontViewController = UINavigationController(rootViewController: view)
        revealController.delegate = appDelegate;
        
        appDelegate.window!.rootViewController = revealController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    //MARK: PARSE ACTIVITY
    func registerLovedOneID()
    {
        self.showActivityIndicator();
        
        //-----------------------------
        //Query Whenter ID exists
        
        if(!self.isInvitedUser)
        {
            let query = PFQuery(className:"Resident")
            query.whereKey("residentId", equalTo:self.textFieldLovedOneID.text!)
            query.whereKey("primary", equalTo:true)
            //query.whereKey("username", equalTo:self.user) //Suggested by Sharad 20/04/2015
            query.findObjectsInBackgroundWithBlock{
                (mainobjects: [AnyObject]?, error: NSError?) -> Void in
                if error == nil
                {
                    // The find succeeded.
                    print("Successfully retrieved \(mainobjects!.count) scores.")
                    if (mainobjects!.count == 0)
                    {
                        self.saveData();
                    }
                    //Count == 0REGISTER Loved one id
                }
                
            }
        }else
        {
            self.saveData()
        }
        //-----------------------------
        
    }
    
    
    func saveData()
    {
        
        
        // Upload data in class "Resident"
        let mediaDetails = PFObject(className:"Resident")
        mediaDetails["username"] = self.user
        
        
        if(self.isInvitedUser)
        {
            mediaDetails["primary"] = false
            mediaDetails["residentId"] = self.lovedOneId
            mediaDetails["name"] =  self.lovedOneName
        }else
        {
            mediaDetails["primary"] = true
            mediaDetails["residentId"] = self.textFieldLovedOneID.text
            mediaDetails["name"] =  self.textFieldName.text
        }
        
        
        mediaDetails.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success)
            {
                self.removeActivityIndicator()
                
                //SETUP
                if(self.isInvitedUser)
                {
                    UserDefault.setUsersResidentId(self.lovedOneId)
                    NSUserDefaults.standardUserDefaults().synchronize()
                }else
                {
                    UserDefault.setUsersResidentId(self.textFieldLovedOneID.text!)
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
                
                
                
                self.pushRecordView()
                
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Successful", message: "We have your email record, You have been successfully registered with your loved one", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                   
                }
                else
                {
                    let alert = UIAlertView(title: "Successful", message: "We have your email record, You have been successfully registered with your loved one", delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
                
                
                
            }
            else
            {
                
                // There was a problem, check error.description
                self.removeActivityIndicator()
                
                //This will get error message sent by Parse SDK.
                let errorObj: AnyObject = error!.userInfo
                let errorString: String = errorObj.valueForKey("error") as! String
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)

                    } else {
                        // Fallback on earlier versions
                    }
                                    }
                else
                {
                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
    }
    
    func getLovedOneIdFromWeb()
    {
        
        
        let query = PFQuery(className:"Invitation")
        //query.whereKey("username", equalTo:self.user) //Suggested by Sharad 20/04/2015
        query.whereKey("email", equalTo:self.user.email!)
        query.findObjectsInBackgroundWithBlock{
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                // The find succeeded.
                if(objects!.count > 0){
                    if let objects = objects as? [PFObject]
                    {
                        for object in objects
                        {
                            //print("++++ \(object.createdAt)")
                            let inviationObject : PFObject = object
                            
                            self.lovedOneId = inviationObject["residentId"] as! String
                            self.isInvitedUser = true;
                            self.textFieldLovedOneID.text = "Loved one ID : \(self.lovedOneId)"
                            self.textFieldLovedOneID.userInteractionEnabled = false;
                            
                            //
                            self.getResidentName(self.lovedOneId as String); //PASS the REsident OBJECT
                        }
                        self.labelMessage.text = "Our record indicates that you are already registered, please continue."
                        
                    }
                }else
                {
                    self.textFieldLovedOneID.text =  self.lovedOneId;
                    
                    //added message for already registered user
                    
                    
                }
                
                self.removeActivityIndicator()
            }
            else
            {
                
                self.removeActivityIndicator()
            }
        }
        
    }
    
    func getResidentName(userLoveOneId :String)
    {
        
        //retrieving loved one name from resident table to show resident name
        let query = PFQuery(className: "Resident")
        
        query.whereKey("residentId", equalTo:userLoveOneId)
        query.whereKey("primary", equalTo:true)
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            if (error == nil && objects?.count != 0)
            {
                for object in objects as! [PFObject!]
                {
                    self.lovedOneName = object["name"] as! String
                    self.textFieldName.text = "Loved One Name : \(self.lovedOneName)"
                    self.textFieldName.userInteractionEnabled = false;
                    
                }
            }
        }
        
        
        
    }
    
    //MARK:- UITextFieldDelegate Methods
    func textFieldDidBeginEditing(textField: UITextField)
    {
        return
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        return
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField.tag == 0
        {
            if(self.isInvitedUser)
            {
                self.endEditing();
            }else
            {
                self.textFieldLovedOneID.becomeFirstResponder()
            }
        }
        else if textField.tag == 1
        {
            self.endEditing();
        }
        
        
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }
    
}//END
