//
//  ManageVideoDetailsViewController.swift
//  AlzhaTV
//
//  Created by QMCPL on 10/04/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit
import Parse
import MobileCoreServices
import CoreFoundation
import MediaPlayer
import AVFoundation

class ManageVideoDetailsViewController: UIViewController, UIPickerViewDataSource, UIAlertViewDelegate, UIPickerViewDelegate
{
    
    @IBOutlet var btnCheckox: UIButton!
    @IBOutlet var lblVideoFrom: UILabel!
    @IBOutlet var imgViewVideoDetails : UIImageView!
    @IBOutlet var imgName:UILabel!
    @IBOutlet var imgUploadedOn:UILabel!
    @IBOutlet var pickerViewFreq: UIPickerView!
    @IBOutlet var labelVideoDuration: UILabel!
    
    var user:PFUser!
    
    var activityIndicator:UIActivityIndicatorView!
    //auto, 15 minutes, 30 minutes, hourly, daily, q 8 hours, q12 hours, or daily
    var frequency = ["Auto", "15 minutes", "30 minutes", "Hourly", "q8 hours", "q12 hours", "Daily"]
    var frequencySelected: String!
    
    var videoDetails:PFObject!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.user = PFUser.currentUser()
        self.pickerViewFreq.delegate = self
        self.pickerViewFreq.dataSource = self
        
        // Do any additional setup after loading the view.
        print("videoDetails \(self.videoDetails)")
        
        self.updateUI()
        self.addUIComponents()
    }
    
    //MARK:- Update values in user interface
    func updateUI()
    {
        //-----------------------------
        //MARK:- Get date on which thumbnail is uploaded from selected PFObject
        //var updatedOn : NSDate = videoDetails["uploadedOn"] as NSDate
        
        let str_Name: String = videoDetails["name"] as! String
//        self.imgName.numberOfLines = 3
        self.lblVideoFrom.text = str_Name
        self.lblVideoFrom.font = UIFont(name: "Arial", size: 16.0)

        
        let updatedOn : NSDate = self.videoDetails.createdAt!
        let dateFormatter1: NSDateFormatter = NSDateFormatter()
        dateFormatter1.dateFormat = "MMM dd, yyyy, HH:mm:ss"
        let str_imgUploadedOn: String = dateFormatter1.stringFromDate(updatedOn)
        print("str_imgUploadedOn \(str_imgUploadedOn)")
        self.imgUploadedOn.text = "Uploaded On               : " + str_imgUploadedOn
        self.imgUploadedOn.font = UIFont(name: "Arial", size: 16.0)
        //-----------------------------
        
//        let str_imgName: String = videoDetails["fileName"] as! String
//        self.imgName.numberOfLines = 3
//        self.imgName.text = str_imgName
//        self.imgName.font = UIFont(name: "Arial", size: 16.0)
        
        //-----------------------------
        //MARK:- Get thumbnail from selected PFObject
        let userImageFile = videoDetails["thumbNail"] as! PFFile
        userImageFile.getDataInBackgroundWithBlock {
            (imageData: NSData?, error: NSError?) -> Void in
            if error == nil
            {
                let image = UIImage(data:imageData!)
                self.imgViewVideoDetails.image = image as UIImage!
            }
        }
        //-----------------------------
        
        let frequencyFromMediaDetails: String = videoDetails["frequency"] as! String
        print("frequencySelected \(frequencyFromMediaDetails)")
        
        switch(frequencyFromMediaDetails)
        {
        case "Auto":
            self.pickerViewFreq.selectRow(0, inComponent: 0, animated: true)
            
        case "Hourly":
            self.pickerViewFreq.selectRow(1, inComponent: 0, animated: true)
            
        case "Daily":
            self.pickerViewFreq.selectRow(2, inComponent: 0, animated: true)
            
        case "Weekly":
            self.pickerViewFreq.selectRow(3, inComponent: 0, animated: true)
            
        case "Monthly":
            self.pickerViewFreq.selectRow(4, inComponent: 0, animated: true)
            
        default:
            self.pickerViewFreq.selectRow(0, inComponent: 0, animated: true)
        }
        
        //VIDEO duration
        let dTotalSeconds : Double = videoDetails["duration"] as! Double
        let dHours = floor(dTotalSeconds / 3600);
        let dMinutes = floor(dTotalSeconds  % 3600 / 60);
        let dSeconds = floor(dTotalSeconds % 3600 % 60);
        
        let stringOfTimeFame : NSString = NSString(format: "Duration                      : %02.f : %02.f : %02.f", dHours, dMinutes, dSeconds);
        labelVideoDuration.text = stringOfTimeFame as String;
    }
    
    func addUIComponents()
    {
        let image = UIImage(named:"delete")
        self.navigationItem.setRightBarButtonItem(UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonItemClicked:"),animated: true)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.darkGrayColor()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        let isdisplay = videoDetails["display"] as! Bool
        
        
        if(!isdisplay)
        {
            self.btnCheckox.setImage(UIImage(named: "unchecked_checkbox"), forState: UIControlState.Normal)
        }
        else
        {
           self.btnCheckox.setImage(UIImage(named: "checked_checkbox"), forState: UIControlState.Normal)
        }

        self.navigationController?.navigationBar.hidden = false
        self.navigationController?.navigationItem.hidesBackButton = false
        self.title = "Video Details"
    }
    
    //MARK:- Delete Button Action
    func barButtonItemClicked(sender: AnyObject)
    {
        //-----
        if (objc_getClass("UIAlertController") != nil)
        {
            if #available(iOS 8.0, *) {
                let alert: UIAlertController = UIAlertController(title: "Delete?", message: "Do you want to delete this record?", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler:{ action in
                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
                    })
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{ action in
                        self.deleteData()
                        self.dismissViewControllerAnimated(true, completion:{ () -> Void in
                        })
                    }))

                }))
                self.presentViewController(alert, animated: true, completion: { () -> Void in
                })
            } else {
                // Fallback on earlier versions
            }
            
                  }
        else
        {
            let alert = UIAlertView(title: "Delete?", message: "Do you want to delete this video?", delegate: self , cancelButtonTitle:"No", otherButtonTitles:"Yes")
            alert.tag = 200
            alert.show()
        }
    }
    
    
    @IBAction func btnCheckBoxClicked(sender: AnyObject) {
        
        
        //self.showActivityIndicator()
        //self.view.userInteractionEnabled = false
        
//        let indexPath : NSIndexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
//        let collectionViewCell:ManageVideosCollectionViewCell = self.collectionView.cellForItemAtIndexPath(indexPath) as! ManageVideosCollectionViewCell
        
       // let mediaDetailObject : PFObject = self.arrayMediaDetails[indexPath.row] as! PFObject
        
        let isdisplay = videoDetails["display"] as! Bool
        
        if (isdisplay)
        {
            self.btnCheckox.setImage(UIImage(named: "unchecked_checkbox"), forState: UIControlState.Normal)
            videoDetails["display"] = false
        }
        else
        {
            self.btnCheckox.setImage(UIImage(named: "checked_checkbox"), forState: UIControlState.Normal)
            videoDetails["display"] = true
//            collectionViewCell.btnCheckBox.setImage(UIImage(named: "checked_checkbox"), forState: UIControlState.Normal)
//            mediaDetailObject["display"] = true
        }
        
        //self.arrayMediaDetails.replaceObjectAtIndex(indexPath.row, withObject: mediaDetailObject)
        videoDetails.saveInBackground()
        
        if btnCheckox.imageForState(UIControlState.Normal) == UIImage(named: "checked_checkbox")
        {
            
            // Make Toast
            self.navigationController?.view.makeToast("Video added to your loved one playlist")
        }
        else
        {
            self.navigationController?.view.makeToast("Video removed from your loved one playlist")
        }
        
        

    }
    
    
    func deleteData()
    {
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false
        
        self.videoDetails.deleteInBackgroundWithBlock{
            (success: Bool, error: NSError?) -> Void in
            if (success)
            {
                // The object has been saved.
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "AlzhaTV", message: "Video deleted successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:{ action in
                            self.dismissViewControllerAnimated(false, completion:{ () -> Void in
                            })
                            self.navigationController?.popViewControllerAnimated(true)
                        }))
                        self.presentViewController(alert, animated: true, completion: { () -> Void in
                        })
                    } else {
                        // Fallback on earlier versions
                    }
                   
                }
                else
                {
                    let alert = UIAlertView(title: "AlzhaTV", message: "Video deleted successfully.", delegate: self , cancelButtonTitle:"Ok")
                    alert.tag = 100
                    alert.show()
                }
            }
            else
            {
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                
                //This will get error message sent by Parse SDK.
                let errorObj: AnyObject = error!.userInfo
                let errorString: String = errorObj.valueForKey("error") as! String
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *)
                    {
                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)

                    }
                    else
                    {
                        // Fallback on earlier versions
                        
                    }
                }
                else
                {
                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
    }
    
    //MARK:- UIPickerView Delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return self.frequency.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return self.frequency[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        //self.showActivityIndicator()
        //self.view.userInteractionEnabled = false
        
        let updatedFreq:String = self.frequency[row]
        print("updatedFreq \(updatedFreq)")
        self.videoDetails["frequency"] = updatedFreq as String
        
        self.videoDetails.saveInBackground()
        /* self.videoDetails.saveInBackgroundWithBlock {
        (success: Bool, error: NSError!) -> Void in
        if (success)
        {
        // The object has been saved.
        self.removeActivityIndicator()
        self.view.userInteractionEnabled = true
        }
        else
        {
        self.removeActivityIndicator()
        self.view.userInteractionEnabled = true
        
        //This will get error message sent by Parse SDK.
        var errorObj: AnyObject = error.userInfo!
        var errorString: String = errorObj.valueForKey("error") as String
        
        if (objc_getClass("UIAlertController") != nil)
        {
        var alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
        var alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
        alert.show()
        }
        }
        }*/
    }
    
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }
    
    //MARK:- UIAlertViewDelegate Method
    func alertView(View: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if View.tag == 100
        {
            switch buttonIndex
            {
            case 0:
                self.navigationController?.popViewControllerAnimated(true)
                
            default:
                print("Default case")
            }
        }
        else if View.tag == 200
        {
            switch buttonIndex
            {
            case 0:
                print("")
                
            case 1:
                self.deleteData()
                
            default:
                print("Default case")
            }
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }
    
}
