////  UserDefault.swift
////  Created by QMCPL on 03/04/15.
//  Copyright (c) 2015 delaPlex Software. All rights reserved.
////
import Foundation

@objc class UserDefault: NSObject
{
    //=======
    class func getUsersResidentId() -> String
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let name = defaults.stringForKey("residentId")
        {
           return(name)
        }
        return ""
    }
    
    class func setUsersResidentId( value: String)
    {
        NSUserDefaults.standardUserDefaults().setValue(value, forKey: "residentId")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    //=======
}