//
//  SubscribePublicVideosViewController.swift
//  AlzhaTV
//
//  Created by QMCPL on 14/04/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit
import Parse
import MobileCoreServices
import CoreFoundation
import MediaPlayer
import AVFoundation

class SubscribePublicVideosViewController: UIViewController, UICollectionViewDelegateFlowLayout, UIAlertViewDelegate
{
   var isDetails: Bool = false
    @IBOutlet var btnCheckShowInfo: UIButton!
    @IBOutlet var viewInfo: UIView!

    @IBOutlet var collectionView : UICollectionView!
    var activityIndicator:UIActivityIndicatorView!
    var user:PFUser!
    var residentId: String!
    var arrayPublicMediaCategory :NSMutableArray = []
    let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.user = PFUser.currentUser()
        
        // Do any additional setup after loading the view.
        self.addUIComponents()
       // self.getData()
    }
    
    func addUIComponents()
    {
        let image = UIImage(named:"imgMyPlateMenu")
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonItemClicked:"),animated: true)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGrayColor()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        if self.isDetails == false
        {
            let arrayOfView: NSArray!
            arrayOfView = self.view.subviews
            print(NSUserDefaults.standardUserDefaults().boolForKey("isInfoPublic"))
            if NSUserDefaults.standardUserDefaults().boolForKey("isInfoPublic") == false
            {
                for item in arrayOfView
                {
                    let view = item as! UIView
                    self.collectionView.backgroundColor = UIColor.blackColor()
                    if view == self.viewInfo
                    {
                        self.viewInfo.layer.borderColor = UIColor.grayColor().CGColor
                        self.viewInfo.layer.borderWidth = 0.5
                        view.bringSubviewToFront(self.view)
                        view.layer.shadowColor = UIColor.grayColor().CGColor//(netHex: 0x9f9f9f).CGColor
                        view.layer.shadowOffset = CGSizeMake(0,4)
                        view.layer.shadowOpacity = 0.5
                        
                        view.alpha = 1
                    }
                        
                        
                    else
                    {
                        view.alpha = 0.2
                    }
                }
                
            }
            else
            {
                self.getData()
            }

        }
       else
        {
             self.getData()
           self.isDetails = false
        }
        self.navigationController?.navigationBar.hidden = false
        self.navigationController?.navigationItem.hidesBackButton = true
        self.title = "Get public video"
    }
    
    @IBAction func actionDonotShow(sender: UIButton)
    {
        sender.selected = !sender.selected
        if sender.selected
        {
            sender.setBackgroundImage(UIImage(named: "checkbox-checked"), forState: UIControlState.Selected)
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isInfoPublic")
            NSUserDefaults.standardUserDefaults().synchronize()
            
        }
        else
        {
            sender.setBackgroundImage(UIImage(named: "checkbox-unchecked"), forState: UIControlState.Normal)
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isInfoPublic")
            NSUserDefaults.standardUserDefaults().synchronize()
            
        }
    }
    @IBAction func actionGotIt(sender: UIButton)
    {
         self.collectionView.backgroundColor = UIColor.lightGrayColor()
        let arrayOfView: NSArray!
        if self.btnCheckShowInfo.selected == true
        {
        }
        
        
        arrayOfView = self.view.subviews
        for item in arrayOfView
        {
            let view = item as! UIView
            
            if view == self.viewInfo 
            {
                view.alpha = 0
            }
            else
            {
                
                view.bringSubviewToFront(self.view)
                view.alpha = 1
            }
            
            
        }
        self.getData()
    }

    
    
    func barButtonItemClicked(sender: AnyObject)
    {
        let revealController:SWRevealViewController = self.revealViewController();
        revealController.revealToggle(sender)
    }
    
    func getData()
    {
        self.arrayPublicMediaCategory.removeAllObjects()
        self.collectionView.reloadData()
        
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false
        
        let query = PFQuery(className:"PublicMediaCategory")
        query.findObjectsInBackgroundWithBlock{
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                if let objects = objects as? [PFObject]
                {
                    for object in objects
                    {
                        //print("++++ \(object.createdAt)")
                        self.arrayPublicMediaCategory.addObject(object)
                    }
                }
                //print("self.arrayMediaDetails \(self.arrayMediaDetails)")
                if (self.arrayPublicMediaCategory.count == 0)
                {
                    self.removeActivityIndicator()
                    self.view.userInteractionEnabled = true
                    
                    if (objc_getClass("UIAlertController") != nil)
                    {
                        if #available(iOS 8.0, *) {
                            let alert = UIAlertController(title: "Alert", message: "No video found.", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)

                        } else {
                            // Fallback on earlier versions
                        }
                                           }
                    else
                    {
                        let alert = UIAlertView(title: "Alert", message: "No video found.", delegate: nil , cancelButtonTitle:"Ok")
                        alert.show()
                    }
                }
                else
                {
                    self.removeActivityIndicator()
                    self.view.userInteractionEnabled = true
                    
                    self.collectionView.reloadData()
                }
            }
            else
            {
                // Log details of the failure
                print("Error: \(error) \(error!.userInfo)")
                // There was a problem, check error.description
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                
                //This will get error message sent by Parse SDK.
                let errorObj: AnyObject = error!.userInfo
                let errorString: String = errorObj.valueForKey("error") as! String
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)

                    } else {
                        // Fallback on earlier versions
                    }
                                   }
                else
                {
                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
    }
    
    /// MARK: UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
            return sectionInsets
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return self.arrayPublicMediaCategory.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ManageVideosCollectionViewCell
        
        cell.imgName.font = UIFont(name: "Arial-BoldMT", size: 14.0)
        cell.imgName.textAlignment = NSTextAlignment.Center
        cell.imgName.textColor = UIColor.darkGrayColor()
        
        let mediaDetailObject : PFObject = self.arrayPublicMediaCategory[indexPath.row] as! PFObject
        print("mediaDetailObject \(mediaDetailObject)")
        
        cell.imgName.text = mediaDetailObject["name"] as? String
        
        let userImageFile = mediaDetailObject["categoryPicture"] as! PFFile
        userImageFile.getDataInBackgroundWithBlock {
            (imageData: NSData?, error: NSError?) -> Void in
            if error == nil
            {
                if let cellToUpdate = collectionView.cellForItemAtIndexPath(indexPath) as? ManageVideosCollectionViewCell
                {
                let image = UIImage(data:imageData!)
                cellToUpdate.imageView.image = image as UIImage!
                }
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        self.isDetails = true
        let storyboard:UIStoryboard = self.storyboard!
        let view:SubscribePublicVideosDetailsViewController = storyboard.instantiateViewControllerWithIdentifier("SubscribePublicVideosDetailsViewController") as! SubscribePublicVideosDetailsViewController
        view.subscribePublicVideoDetails = self.arrayPublicMediaCategory[indexPath.row] as! PFObject
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }

}
