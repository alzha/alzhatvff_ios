//
//  RegisterViewController.swift
//  AlzhaTV
//
//  Created by QMCPL on 02/04/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class RegisterViewController: UIViewController, UITextFieldDelegate, UIAlertViewDelegate
{
    @IBOutlet var txtFieldRegisterUsername: UITextField!
    @IBOutlet var txtFieldRegisterEmail: UITextField!
    @IBOutlet var txtFieldRegisterPassword: UITextField!
    @IBOutlet var txtFieldRegisterRePassword: UITextField!
    @IBOutlet var btnRegister: UIButton!
    
    var activityIndicator:UIActivityIndicatorView!
    
    var user:PFUser!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addUIComponents()
    }
    
    func addUIComponents()
    {
        self.txtFieldRegisterUsername.layer.borderWidth = 0.5
        self.txtFieldRegisterUsername.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldRegisterUsername.textColor = UIColor.darkGrayColor()
        self.txtFieldRegisterUsername.delegate = self
        self.txtFieldRegisterUsername.returnKeyType = UIReturnKeyType.Next
        
        self.txtFieldRegisterEmail.layer.borderWidth = 0.5
        self.txtFieldRegisterEmail.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldRegisterEmail.textColor = UIColor.darkGrayColor()
        self.txtFieldRegisterEmail.delegate = self
        self.txtFieldRegisterEmail.returnKeyType = UIReturnKeyType.Next
        self.txtFieldRegisterEmail.keyboardType = UIKeyboardType.EmailAddress
        
        self.txtFieldRegisterPassword.layer.borderWidth = 0.5
        self.txtFieldRegisterPassword.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldRegisterPassword.textColor = UIColor.grayColor()
        self.txtFieldRegisterPassword.delegate = self
        self.txtFieldRegisterPassword.returnKeyType = UIReturnKeyType.Next
        self.txtFieldRegisterPassword.secureTextEntry = true
        
        self.txtFieldRegisterRePassword.layer.borderWidth = 0.5
        self.txtFieldRegisterRePassword.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldRegisterRePassword.textColor = UIColor.grayColor()
        self.txtFieldRegisterRePassword.delegate = self
        self.txtFieldRegisterRePassword.returnKeyType = UIReturnKeyType.Done
        self.txtFieldRegisterRePassword.secureTextEntry = true
        
        self.btnRegister.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)
        self.btnRegister.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnRegister.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
        self.btnRegister.layer.cornerRadius = 10.0
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBar.hidden = false
        self.title = "Register A New User"
        
        self.user = PFUser.currentUser()
        //print("PFUser.currentUser() +++ \(PFUser.currentUser())")
         print("user.username +++ \(user.username)")
        
        self.view.userInteractionEnabled = true
        
        //This will reset all textfields.
        self.txtFieldRegisterUsername.text = ""
        self.txtFieldRegisterEmail.text = ""
        self.txtFieldRegisterPassword.text = ""
        self.txtFieldRegisterRePassword.text = ""
    }
    

    //MARK: - Email address validations.
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let range = testStr.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
        let result = range != nil ? true : false
        print("Email resul  \(result)")
        return result
    }
    
    //MARK:- Register Button Action
    @IBAction func btnRegisterClicked(sender: AnyObject)
    {
      let result = self.checkFieldsComplete()
        
        if(result == "" || result.isEmpty)
        {
            self.registerNewUser()
        }
        else
        {
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *)
                {
                    let alert = UIAlertController(title: "Alert", message: result, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                } else
                {
                    // Fallback on earlier versions
                }
            }
            else
            {
                let alert = UIAlertView(title: "Alert", message: result, delegate: nil , cancelButtonTitle:"Ok")
                alert.show()
            }
        }
    }

    //MARK:- Checking for empty textfields
    func checkFieldsComplete() ->String
    {
        if(self.txtFieldRegisterUsername.text == "" || self.txtFieldRegisterUsername.text!.isEmpty || self.txtFieldRegisterEmail.text == "" || self.txtFieldRegisterEmail.text!.isEmpty || self.txtFieldRegisterPassword.text == "" || self.txtFieldRegisterPassword.text!.isEmpty || self.txtFieldRegisterRePassword.text == "" || self.txtFieldRegisterRePassword.text!.isEmpty)
        {
            return "You need to complete all fields."
        }
        else if(!self.isValidEmail(self.txtFieldRegisterEmail.text!))
        {
            return "Email is invalid."
        }
        else if(!(self.txtFieldRegisterPassword.text == self.txtFieldRegisterRePassword.text))
        {
            return "Passwords don't match."
        }
        else
        {
            return ""
        }
    }
    
    //MARK:- Register to PFUser
    func registerNewUser()
    {
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false
        
        let newUser:PFUser = PFUser()
        newUser.username = self.txtFieldRegisterEmail.text
        newUser.setObject(self.txtFieldRegisterUsername.text!, forKey: "name");
        newUser.email = self.txtFieldRegisterEmail.text!.lowercaseString
        newUser.password = self.txtFieldRegisterPassword.text
        newUser.signUpInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if !(error != nil)
            {
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                
                let storyboard:UIStoryboard = self.storyboard!
                let view:LovedOneIDViewController = storyboard.instantiateViewControllerWithIdentifier("LovedOneIDViewController") as! LovedOneIDViewController
                self.navigationController?.pushViewController(view, animated: true)
                
                print("newUser.objectId \(newUser.objectId)")
            }
            else
            {
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                
                //This will get error message sent by Parse SDK.
                let errorObj: AnyObject = error!.userInfo
                let errorString: String = errorObj.valueForKey("error") as! String
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                   
                }
                else
                {
                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
    }
    
    //MARK:- UITextFieldDelegate Methods
    func textFieldDidBeginEditing(textField: UITextField)
    {
        return
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        return
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == self.txtFieldRegisterUsername
        {
            self.txtFieldRegisterEmail.becomeFirstResponder()
        }
        else if textField == self.txtFieldRegisterEmail
        {
            self.txtFieldRegisterPassword.becomeFirstResponder()
        }
        else if textField == self.txtFieldRegisterPassword
        {
            self.txtFieldRegisterRePassword.becomeFirstResponder()
        }
        else
        {
            self.btnRegisterClicked("")
            textField.resignFirstResponder()
        }
        return true
    }
    
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }
}
