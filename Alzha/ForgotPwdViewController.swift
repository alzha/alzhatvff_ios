//
//  ForgotPwdViewController.swift
//  AlzhaTV
//
//  Created by Indrayani on 07/03/16.
//  Copyright © 2016 QMC243-MAC-ShrikantP. All rights reserved.
//

import UIKit
import Parse

class ForgotPwdViewController: UIViewController,UIAlertViewDelegate,UITextFieldDelegate {

    @IBOutlet var txtEmailId: UITextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()
self.txtEmailId.delegate = self
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.hidden = false

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnSubmitClicked(sender: AnyObject)
    {
      let strEmail = self.txtEmailId.text
       PFUser.requestPasswordResetForEmailInBackground(strEmail!) {(succeeded, error) -> Void in
        if error == nil {
            if succeeded { // SUCCESSFULLY SENT TO EMAIL
                print("Reset email sent to your inbox");
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Success", message: "Reset email sent to your inbox", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (alert) -> Void in
                             self.navigationController?.popViewControllerAnimated(true)
                        }))
                        //self.presentViewController(alert, animated: true, completion: nil)
                      self.presentViewController(alert, animated: true, completion: nil)
                    }
                    else {
                        // Fallback on earlier versions
                    }
                    
                    
                }
                else
                {
                    let alert = UIAlertView(title: "Success", message: "Reset email sent to your inbox", delegate: self , cancelButtonTitle:"Ok")
                    alert.show()
                }

            }
            else {
                // SOME PROBLEM OCCURED
            }
        }
        else { //ERROR OCCURED, DISPLAY ERROR MESSAGE
            print(error!.description);
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    //self.presentViewController(alert, animated: true, completion: nil)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    // Fallback on earlier versions
                }
                
                
            }
            else
            {
                let alert = UIAlertView(title: "Error", message: error?.localizedDescription, delegate: nil , cancelButtonTitle:"Ok")
                alert.delegate = self
                alert.show()
            }

        }
    }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        
        if buttonIndex == 0
        {
               self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.txtEmailId.resignFirstResponder()
        return true
        
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
