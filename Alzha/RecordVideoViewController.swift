//
//  RecordVideoViewController.swift
//
//  Copyright 2011-present Parse Inc. All rights reserved.
//

import UIKit
import Parse
import MobileCoreServices
import CoreFoundation
import MediaPlayer
import AVFoundation

class RecordVideoViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, MenuViewControllerDelegate, UIAlertViewDelegate
{
    let uploadRequest = AWSS3TransferManagerUploadRequest();

    @IBOutlet var lblPercentage: UILabel!
    @IBOutlet var btnRecordVideo: UIButton!
    @IBOutlet var imgViewThumbnail:UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet var statusLabel:UILabel!
    var isCameraClicked:Bool =  false
    var isComingFromLogin: Bool = false
    var activityIndicator:UIActivityIndicatorView!
    var user:PFUser!
    var progressVal : Float = 0.0
    var progressPercentage:Float = 0.0
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.user = PFUser.currentUser()
    }
    // New Doc Dir
    func getThumbnailFromDir()
    {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let getImagePath = paths.stringByAppendingString("/thumbNail.png")
        
        
        let checkValidation = NSFileManager.defaultManager()
        
        if (checkValidation.fileExistsAtPath(getImagePath))
        {
            print("FILE AVAILABLE");
            self.imgViewThumbnail.image = UIImage(data: NSData(contentsOfURL: NSURL(fileURLWithPath:getImagePath))!)
        }
        
        
    }
    func saveThumbnailToDir(thumb : UIImage)
    {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let getImagePath = paths.stringByAppendingString("/thumbNail.png")
        
        
        let checkValidation = NSFileManager.defaultManager()
        
        if (checkValidation.fileExistsAtPath(getImagePath))
        {
            print("FILE AVAILABLE");
            do{
                try checkValidation.removeItemAtPath(getImagePath)
                UIImageJPEGRepresentation(thumb, 0.7)?.writeToFile(getImagePath, atomically: true)

            }catch{
                
            }
        }
        else
        {
            UIImageJPEGRepresentation(thumb, 0.7)?.writeToFile(getImagePath, atomically: true)
            print("FILE NOT AVAILABLE");
        }
        
    }

    func addUIComponentsForUploading()
    {
         repeat
        {
            
            if NSUserDefaults.standardUserDefaults().valueForKey("progressVal") != nil
            {
                self.progressVal = NSUserDefaults.standardUserDefaults().valueForKey("progressVal") as! Float
                if  NSUserDefaults.standardUserDefaults().valueForKey("progressPercentage") != nil
                {
                    self.progressPercentage = NSUserDefaults.standardUserDefaults().valueForKey("progressPercentage") as! Float
                    
                    print("self.progressVal \(self.progressVal)")
                    print("progressPercentage \(progressPercentage)")
                }
            }
           
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in

                self.statusLabel.text = "Uploading..."
                self.progressView.progress = self.progressVal
                if self.progressPercentage <= 100
                {
                    self.lblPercentage.text = String(format: "%.2f", self.progressPercentage) + "%"
                }
            })
         }   while (NSUserDefaults.standardUserDefaults().boolForKey("isUploaded") == true)
        // as String//String(format: "%.2f", self.progressPercentage) + "%"
        
        print("DOne")
        if NSUserDefaults.standardUserDefaults().boolForKey("isUploaded") == false
        {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("orientationChanged:"), name: UIDeviceOrientationDidChangeNotification, object: nil)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.addUIComponents()
                self.lblPercentage.alpha = 0
                self.progressView.alpha = 0
                self.statusLabel.alpha = 0
                self.btnRecordVideo.alpha = 1
                self.imgViewThumbnail.image = UIImage(named: "HoldYourCamera.jpeg")
            })
            
            
            if NSUserDefaults.standardUserDefaults().valueForKey("errorString") != nil
            {
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Error", message: NSUserDefaults.standardUserDefaults().valueForKey("errorString") as? String, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                    
                }
                else
                {
                    let alert = UIAlertView(title: "Error", message: NSUserDefaults.standardUserDefaults().valueForKey("errorString") as? String, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
            else
            {
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "AlzaTV", message: "Video added successfully", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                    
                }
                else
                {
                    let alert = UIAlertView(title: "AlzaTV", message: "Video added successfully", delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
        NSUserDefaults.standardUserDefaults().removeObjectForKey("progressPercentage")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("progressVal")
        NSUserDefaults.standardUserDefaults().synchronize()
       
    }

    override func viewWillDisappear(animated: Bool) {
        
        
    }
    override func viewDidDisappear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    func addUIComponents()
    {
        self.lblPercentage.alpha = 0
        self.progressView.alpha = 0
        self.statusLabel.alpha = 0

        self.btnRecordVideo.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)//UIColor.darkGrayColor()
        self.btnRecordVideo.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnRecordVideo.layer.cornerRadius = 10.0
        self.btnRecordVideo.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
        
        self.statusLabel.textColor = UIColor.purpleColor()
        self.statusLabel.textAlignment = NSTextAlignment.Center
        self.statusLabel.layer.cornerRadius = 10.0
        
        let image = UIImage(named:"imgMyPlateMenu")
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonItemClicked:"),animated: true)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGrayColor()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        if NSUserDefaults.standardUserDefaults().boolForKey("isUploaded") == false
        {
            self.navigationController?.navigationBar.hidden = false
            self.navigationController?.navigationItem.hidesBackButton = true
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("orientationChanged:"), name: UIDeviceOrientationDidChangeNotification, object: nil)
            self.addUIComponents()
            
        }
        else
        {
            // fetching Thumbnail from Doc Dir
            getThumbnailFromDir()

            self.lblPercentage.alpha = 1
            self.progressView.alpha = 1
            self.statusLabel.alpha = 1
            self.btnRecordVideo.alpha = 0
//            self.imgViewThumbnail.alpha = 0
            self.navigationController?.navigationBar.hidden = false
            self.navigationController?.navigationItem.hidesBackButton = true
            let image = UIImage(named:"imgMyPlateMenu")
            self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonItemClicked:"),animated: true)
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGrayColor()
            let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
            dispatch_async(queue, { () -> Void in
                
                self.addUIComponentsForUploading()
            })
            
        }

       
        self.title = "Make a video"
    }
    
    func barButtonItemClicked(sender: AnyObject)
    {
        
        let revealController:SWRevealViewController = self.revealViewController();
        revealController.revealToggle(sender)

    }
    
    //MARK:- Record Button Action..directly connected from storyboard
    @IBAction func btnRecordVideoClicked(sender: AnyObject)
    {
   //UIDevice.currentDevice().orientation =
        switch UIDevice.currentDevice().orientation
        {
        case .Portrait:
            //print("Camera not available.")
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Alert", message: "Please hold your device in Landscape", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)

                } else {
                    // Fallback on earlier versions
                }
                            }
            else
            {
                let alert = UIAlertView(title: "Alert", message: "Please hold your device in Landscape", delegate: nil , cancelButtonTitle:"Ok")
                alert.show()
            }
        case .PortraitUpsideDown:
            //print("Camera not available.")
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Alert", message: "Please hold your device in Landscape", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)

                } else {
                    // Fallback on earlier versions
                }
                            }
            else
            {
                let alert = UIAlertView(title: "Alert", message: "Please hold your device in Landscape", delegate: nil , cancelButtonTitle:"Ok")
                alert.show()
            }
        case .LandscapeLeft:
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
            {
                //print("captureVideoPressed and camera available.")
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .Camera;
                imagePicker.mediaTypes = [kUTTypeMovie as String]
                imagePicker.allowsEditing = false
                imagePicker.videoMaximumDuration = NSTimeInterval(7)
                imagePicker.showsCameraControls = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
            else
            {
                //print("Camera not available.")
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Alert", message: "Camera not available.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)

                    } else {
                        // Fallback on earlier versions
                    }
                                   }
                else
                {
                    let alert = UIAlertView(title: "Alert", message: "Camera not available.", delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }

        case .LandscapeRight:
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
            {
                //print("captureVideoPressed and camera available.")
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .Camera;
                imagePicker.mediaTypes = [kUTTypeMovie as String]
                imagePicker.allowsEditing = false
                imagePicker.showsCameraControls = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
            else
            {
                //print("Camera not available.")
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Alert", message: "Camera not available.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)

                    } else {
                        // Fallback on earlier versions
                    }
                    
                }
                else
                {
                    let alert = UIAlertView(title: "Alert", message: "Camera not available.", delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }

        default:
           print("default")
           if (objc_getClass("UIAlertController") != nil)
           {
            if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: "Alert", message: "Please hold your device in Landscape", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
            } else {
                // Fallback on earlier versions
            }
           }
           else
           {
            let alert = UIAlertView(title: "Alert", message: "Please hold your device in Landscape", delegate: nil , cancelButtonTitle:"Ok")
            alert.show()
            }

        }
        
        
        
        
            }
    
    //MARK:- ImagePickerController Delegate Methods
    //func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info:NSDictionary!)
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject])
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isUploaded")
        NSUserDefaults.standardUserDefaults().synchronize()
        self.isCameraClicked = true
        
        self.lblPercentage.alpha = 1
        self.statusLabel.alpha = 1
        self.btnRecordVideo.alpha = 0
        self.statusLabel.text = "Connecting..."
        self.progressView.alpha = 1
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false
        
        //-----------------------------
        //MARK:- Generate key for image
        let uuid = NSUUID().UUIDString
        
        //This will save recorded video to Album
       // let tempImage = info[UIImagePickerControllerMediaURL] as! NSURL!
      //  let pathString = tempImage.relativePath
        self.dismissViewControllerAnimated(true, completion: {})
        
        
        //-----------------------------
        //MARK:- Save recorded video to Documents
        let videoUrl: NSURL =
        (info as NSDictionary).objectForKey(UIImagePickerControllerMediaURL) as! NSURL
    
       let videoData: NSData = NSData(contentsOfURL: videoUrl)!
        let paths: NSArray = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsPath: NSString = paths.objectAtIndex(0) as! NSString
        //-----------------------------
        
        //-----------------------------
        //MARK:- Save recorded video name as per current date-time format
        let date : NSDate = NSDate()
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
        let videoName: String = dateFormatter.stringFromDate(date)
        let str_VideoName : String = "VID_\(videoName)"
        //print("str_VideoName \(str_VideoName)")
        
        let docPathVideo: NSString = documentsPath.stringByAppendingPathComponent("/\(str_VideoName).mov")
       var success: Bool = videoData.writeToFile(docPathVideo as String, atomically: false)
        print("docPathVideo ---> \(docPathVideo)")
        //-----------------------------
        
        //-----------------------------
        //MARK:- Generate thumbnail of a video.
        let asset:AVURLAsset = AVURLAsset(URL: videoUrl, options: nil)
        let gen:AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        gen.appliesPreferredTrackTransform = true
        let time:CMTime = CMTimeMakeWithSeconds(0,30)
        //var error:NSError?
        var image:CGImageRef!
        var thumb:UIImage!
        var imageData : NSData!
                   do
            {
                
                image   =  try gen.copyCGImageAtTime(time, actualTime: nil)//gen.copyCGImageAtTime(time, actualTime:nil, error: &error)
            }
            catch
            {
                
            }
            thumb = UIImage(CGImage: image)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.imgViewThumbnail.image = thumb as UIImage!
            })
            
           imageData  = UIImagePNGRepresentation(thumb)
            
        

        //-----------------------------
        
        //-----------------------------
        //MARK:- Generate and save thumbnail in .png fromat
//        let pngPath: String = NSHomeDirectory().stringByAppendingString("Documents/thumbNail.png")//stringByAppendingPathComponent("Documents/thumbNail.png")
//        UIImagePNGRepresentation(thumb)!.writeToFile(pngPath, atomically: true)

        // Saving thumbnail to document directory
        
        self.saveThumbnailToDir(thumb)
        
        // Create file manager
        //var error1: NSError?
       // var fileMgr:NSFileManager = NSFileManager.defaultManager()
        
        // Point to Document directory
        let docDir  :String = NSHomeDirectory().stringByAppendingString("Documents")//stringByAppendingPathComponent("Documents")
        print("docDirThumbnail \(docDir)")
        // Write out the contents of home directory to console
      //  print("Documents directory: \(fileMgr.contentsOfDirectoryAtPath(docDir, error: &error1))")
        //-----------------------------
        
        //-----------------------------
        //MARK:- Check whether video exist in Docments or not.
        let existsThumbnail: Bool = NSFileManager.defaultManager().fileExistsAtPath(docDir)
        if(existsThumbnail)
        {
            print("Thumbnail exists")
        }
        else
        {
            print("Thumbnail doesn't exist")
        }
        //-----------------------------
        
        //------------ MOV to MP4-------
        
        print("Ashu: docPathVideo >>>> \(docPathVideo)")
        let avAsset = AVURLAsset(URL: NSURL.fileURLWithPath(docPathVideo as String), options: nil)
        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
        exportSession!.outputFileType = AVFileTypeMPEG4
//       let size =  exportSession!.estimatedOutputFileLength
        let docPathVideo1: NSString = documentsPath.stringByAppendingPathComponent("/\(str_VideoName).mp4")
        exportSession!.outputURL = NSURL.fileURLWithPath(docPathVideo1 as String)
        exportSession!.exportAsynchronouslyWithCompletionHandler { () -> Void in
            // NSFileManager.defaultManager().removeItemAtPath(self.filePath(self.mov_extenstion), error: nil)
            
         switch exportSession!.status {
            case AVAssetExportSessionStatus.Completed:
                print("Completed")
         //MARK:- Upload video to Amazon server.
        let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "us-east-1:631186b9-404c-4341-91f3-230e686b5993")
        let configuration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: credentialProvider)
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = configuration
        
        print("before upload \(docPathVideo1)")
        
        let uploadFileURL = NSURL.fileURLWithPath(docPathVideo1 as String)
        
                self.uploadRequest.bucket = "alzha";
        self.uploadRequest.key = uuid + ".mp4";
        //print("uploadRequest.key \(uploadRequest.key)")
        self.uploadRequest.body = uploadFileURL;
                var totalSize: Double!

               
        self.uploadRequest.uploadProgress =
            { (bytesSent:Int64, totalBytesSent:Int64,  totalBytesExpectedToSend:Int64) -> Void in
                totalSize  = Double(totalBytesSent)/1048576.00
                totalSize = round(totalSize)
            self.progressVal = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
            self.progressPercentage = self.progressVal * 100
            
           // print("progress\(self.progressVal)")
            
                
                let queue  =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                dispatch_sync(queue, {() -> Void in
                self.lblPercentage.alpha = 1
                self.progressView.alpha = 1
                self.statusLabel.alpha = 1
                self.imgViewThumbnail.alpha = 1
                self.progressView.progress = self.progressVal
                NSUserDefaults.standardUserDefaults().setValue(self.progressVal, forKey: "progressVal")
                    
                NSUserDefaults.standardUserDefaults().synchronize()
                self.statusLabel.text = "Uploading..."
                
                if self.progressPercentage <= 100
                {
                    self.lblPercentage.text = String(format: "%.2f", self.progressPercentage) + "%"
                    NSUserDefaults.standardUserDefaults().setValue( self.progressPercentage, forKey: "progressPercentage")
                    NSUserDefaults.standardUserDefaults().synchronize()

                }
                //print("totalBytesSent \(totalBytesSent)")
            })
        }
        
        let S3TransferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        S3TransferManager.upload(self.uploadRequest).continueWithExecutor(BFExecutor.mainThreadExecutor(), withBlock:{
            (task: BFTask!) -> BFTask! in
            
            if(task.error != nil)
            {
                let errorObj: AnyObject = task.error.userInfo
                print("S3 error whille uploading ++++ \(errorObj)")
                
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    // var alert = UIAlertController(title: "", message:  "Error while uploading video.", preferredStyle: UIAlertControllerStyle.Alert)
                    //alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    //self.presentViewController(alert, animated: true, completion: nil)
                }
                else
                {
                    let alert = UIAlertView(title: "Error", message:  "Error while uploading video.", delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
            else
            {
                print("upload succesfully")
                let strName: String = self.user.objectForKey("name") as! String//self.user.username!
                //-----------------------------
                //MARK:- Upload data in class "Media Details"
                let imageFile = PFFile(name:"thumbNail.png", data:imageData!)
                let mediaDetails = PFObject(className:"MediaDetails")
                mediaDetails["residentId"] = UserDefault.getUsersResidentId()
                mediaDetails["thumbNail"] = imageFile
                mediaDetails["dailyUpload"] = true
                mediaDetails["display"] = true
                mediaDetails["keyS3"] = uuid + ".mp4"
                mediaDetails["username"] = self.user
                mediaDetails["name"] = "Video By " + strName
                mediaDetails["fileName"] = str_VideoName
                mediaDetails["frequency"] = "Auto"
                mediaDetails["size"] = totalSize
                mediaDetails["fileUploaded"] = true
                
                //Ashvini : setting ACL access to videos for both invited and current user
                let acl = PFACL()
                acl.setPublicWriteAccess(true)
                acl.setPublicReadAccess(true)
                mediaDetails.ACL = acl
                
                //MARK: YOGESH
                //FIXME NEED TO BE TESTED & duration PUSH TO PARSE June 18, 2015
                let pathURL : NSURL = NSURL(fileURLWithPath: docPathVideo as String);
                let videoDuration : AVURLAsset = AVURLAsset(URL: pathURL, options: nil);
                let duration : CMTime = videoDuration.duration;
                let dTotalSeconds : Double = CMTimeGetSeconds(duration)
                
                mediaDetails["duration"] = dTotalSeconds
                //END OF LOGICE June 18, 2015
                
                mediaDetails.saveInBackgroundWithBlock {
                    (success: Bool, error: NSError?) -> Void in
                    if (success)
                    {
                        self.removeActivityIndicator()
                        self.view.userInteractionEnabled = true
                        
                        if (objc_getClass("UIAlertController") != nil)
                        {
                            if #available(iOS 8.0, *) {
                                let alert = UIAlertController(title: "AlzhaTV", message: "Video Added Successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                                self.presentViewController(alert, animated: true, completion: nil)
                            } else {
                                // Fallback on earlier versions
                            }
                           
                        }
                        else
                        {
                            let alert = UIAlertView(title: "AlzhaTV", message: "Video Added Successfully.", delegate: nil , cancelButtonTitle:"Ok")
                            alert.show()
                        }
                        self.statusLabel.text = "Uploaded Successfully..."
                        //-----------------------------
                        
                        //MARK:- Remove Video if exists
                        if(NSFileManager.defaultManager().fileExistsAtPath(docPathVideo as String))
                        {
                            print("MOV Video removed")
                          //  NSFileManager.defaultManager().removeItemAtPath(docPathVideo as String, error: nil)
                            do
                            {
                                try NSFileManager.defaultManager().removeItemAtPath(docPathVideo as String)
                            
                            }
                            catch
                            {
                                
                            }
                        }
                        
                        if(NSFileManager.defaultManager().fileExistsAtPath(docPathVideo1 as String))
                        {
                            print("MP4 Video removed")
                         //   NSFileManager.defaultManager().removeItemAtPath(docPathVideo1 as String, error: nil)
                            do
                            {
                                try NSFileManager.defaultManager().removeItemAtPath(docPathVideo1 as String)
                                
                            }
                            catch
                            {
                                
                            }
                        }
                        //-----------------------------
                        
                        //MARK:- Remove Thumbnail if exists
                        
                        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
                        let getImagePath = paths.stringByAppendingString("/thumbNail.png")
                        
                        
                        let checkValidation = NSFileManager.defaultManager()
                        
                        if (checkValidation.fileExistsAtPath(getImagePath))
                        {
                            print("FILE AVAILABLE");
                            do{
                                try checkValidation.removeItemAtPath(getImagePath)
                                
                            }catch{
                                
                            }
                        }

                        //-----------------------------
                        self.progressView.alpha = 0
                        self.statusLabel.alpha = 0
                        self.lblPercentage.alpha = 0
                        self.imgViewThumbnail.alpha = 1
                        self.imgViewThumbnail.image = UIImage(named: "HoldYourCamera.jpeg")
                        self.btnRecordVideo.alpha = 1
                       
                    }
                    else
                    {
                        self.progressView.alpha = 0
                        self.statusLabel.alpha = 0
                        self.lblPercentage.alpha = 0
                        self.imgViewThumbnail.alpha = 1
                        self.imgViewThumbnail.image = UIImage(named: "HoldYourCamera.jpeg")
                        // There was a problem, check error.description
                        self.removeActivityIndicator()
                        self.view.userInteractionEnabled = true
                         self.btnRecordVideo.alpha = 1
                        //This will get error message sent by Parse SDK.
                        let errorObj: AnyObject = error!.userInfo
                        let errorString: String = errorObj.valueForKey("error") as! String
                        
                        if (objc_getClass("UIAlertController") != nil)
                        {
                            if #available(iOS 8.0, *) {
                                let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                                self.presentViewController(alert, animated: true, completion: nil)

                            } else {
                                // Fallback on earlier versions
                            }
                                                    }
                        else
                        {
                            let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                            alert.show()
                        }
                    }
                    NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isUploaded")
                    NSUserDefaults.standardUserDefaults().synchronize()

                }
                
                //-----------------------------
            }
           

            return nil
        })
        
        //coverter ends here
                
                break
            case AVAssetExportSessionStatus.Failed:
                exportSession?.error?.localizedDescription
                print("Failed \(exportSession?.error?.localizedDescription)")
                break
                //case AVAssetExportSessionStatus.Cancelled:
                //    　print("Cancelled")
            default:
                break
            }
        }
        
    
    }
    
    func image(image: UIImage, didFinishSavingWithError
        error: NSErrorPointer, contextInfo:UnsafePointer<Void>)
    {
        if error != nil
        {
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Error", message: "video Saving Failed", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
               
            }
            else
            {
                let alert = UIAlertView(title: "Error", message: "Video Saving Failed.", delegate: nil , cancelButtonTitle:"Ok")
                alert.show()
            }
        }
        else
        {
            //self.showActivityIndicator()
            //self.view.userInteractionEnabled = false
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK:- Hide Status Bar
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool)
    {
        UIApplication.sharedApplication().statusBarHidden = true
    }
    
    override func childViewControllerForStatusBarHidden() -> UIViewController?
    {
        return nil
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    //----------------------
    
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.center = self.view.center
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
      //  self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

  
    
    func orientationChanged(notification:NSNotification)
    {
         self.adjustViewsForOrientation(UIDevice.currentDevice().orientation)        //
    }
    func adjustViewsForOrientation(orientation: UIDeviceOrientation)
    {
        
        //UIDevice.currentDevice().orientation =
        switch orientation
        {
        case .LandscapeLeft:
            self.landscapeAction()
  
            /*if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Start", message: "Now you can record video in landscape mode", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style:  UIAlertActionStyle.Default, handler: { (alert) -> Void in
                       self.landscapeAction()
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style:  UIAlertActionStyle.Default, handler:nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
                
            }
            else
            {
                let alert = UIAlertView(title: "Start", message: "Now you can record video in landscape mode", delegate: self , cancelButtonTitle:"Ok")
                alert.tag = 1990
                alert.show()
            }
            */

            
        case .LandscapeRight:
             self.landscapeAction()
            /*if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Start", message: "Now you can record video in landscape mode", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style:  UIAlertActionStyle.Default, handler: { (alert) -> Void in
                        self.landscapeAction()
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style:  UIAlertActionStyle.Default, handler:nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
                
            }
            else
            {
                let alert = UIAlertView(title: "Start", message: "Now you can record video in landscape mode", delegate: self , cancelButtonTitle:"Ok",otherButtonTitles: "Cancel")
                alert.tag = 1990
                alert.show()
            }*/
        default:
            print("default")
            
        }
        

    }

  
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.tag == 1990
        {
            if buttonIndex == 0
            {
               self.landscapeAction()
                
            }
            else
            {
                alertView.dismissWithClickedButtonIndex(1, animated: true)
            }
            
        }
        
    }
    
    func landscapeAction()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            //print("captureVideoPressed and camera available.")
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .Camera;
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            imagePicker.allowsEditing = false
            imagePicker.showsCameraControls = true
           imagePicker.videoMaximumDuration =  NSTimeInterval(420)
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //print("Camera not available.")
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Alert", message: "Camera not available.", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                } else {
                    // Fallback on earlier versions
                }
            }
            else
            {
                let alert = UIAlertView(title: "Alert", message: "Camera not available.", delegate: nil , cancelButtonTitle:"Ok")
                alert.tag = 1990
                alert.show()
            }
        }

    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        
        return .All
    }
    
    
    
    }

