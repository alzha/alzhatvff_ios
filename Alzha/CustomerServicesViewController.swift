//
//  CustomerServicesViewController.swift
//  AlzhaTV
//
//  Created by QMCPL_MAC2 on 10/08/15.
//  Copyright (c) 2015 QMC243-MAC-ShrikantP. All rights reserved.
//

import UIKit
import Parse
class CustomerServicesViewController: UIViewController,UITextFieldDelegate, UITextViewDelegate

{
    var activityIndicator:UIActivityIndicatorView!
    var user: PFUser!
    var objectID : String!
    
    @IBOutlet var txtSubject: UITextField!
    
    @IBOutlet var txtMessage: UITextView!
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = false
        self.navigationController?.navigationItem.hidesBackButton = false
        self.title = "Customer Service"
        
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        txtMessage.delegate = self
        txtSubject.delegate = self
        addBackButton()
        
        // Do any additional setup after loading the view.
        self.user = PFUser.currentUser()
        print("user.username \(self.user.username)")
    }

    func barButtonItemClicked(sender: AnyObject)
    {
         self.view.endEditing(true)
        let revealController:SWRevealViewController = self.revealViewController();
        revealController.revealToggle(sender)
    }
    
    
    //MARK: Initial Setup
    func addBackButton()
    {
        let image = UIImage(named:"imgMyPlateMenu")
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonItemClicked:"),animated: true)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGrayColor()
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

  
    
 @IBAction func submitAction(sender: UIButton)
{
    
    if(self.txtSubject.text == "" || self.txtMessage.text == "") {
        if #available(iOS 8.0, *) {
            let alert = UIAlertController(title: "AlzhaTV", message: "Please fill all fields ", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)

        } else {
            // Fallback on earlier versions
        }
           }
    else {
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false

    
        let mediaDetails = PFObject(className:"CustomerService")
        mediaDetails["subject"] = txtSubject.text
        mediaDetails["text"] = txtMessage.text
        mediaDetails["username"] = self.user
        //.valueForKey("objectId") as! String
        
        mediaDetails.saveInBackgroundWithBlock
            {
            (success: Bool, error: NSError?) -> Void in
            if(success) {
                self.removeActivityIndicator()
                 self.view.userInteractionEnabled = true
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "AlzhaTV", message: "Message Sent Successfully", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)

                } else {
                    // Fallback on earlier versions
                }
                           }
            
            
        }
        
    }
    
    }
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }
    
    //MARK:- UITextFieldDelegate Methods
    func textFieldDidBeginEditing(textField: UITextField)
    {
        
        return
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        
        return
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == self.txtSubject
        {
            self.txtMessage.becomeFirstResponder()
        }
        else
        {
            //self.submitAction("")
            textField.resignFirstResponder()
        }
            
        
        
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    func textViewDidBeginEditing(textView: UITextView) {
        if(textView.text == "Your Message")
        {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        
        if textView == txtMessage
        {
            if textView.text.characters.count == 0
            {
                textView.text = "Your Message"
                textView.textColor = UIColor.lightGrayColor()
            }
        }
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }

}//@end


