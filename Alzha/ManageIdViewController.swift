//
//  ManageIdViewController.swift
//  AlzhaTV
//
//  Created by Yogesh on 19/06/15.
//  Copyright (c) 2015 QMC243-MAC-ShrikantP. All rights reserved.
//

import UIKit
import Parse
class ManageIdViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var txtFieldUserLovedOneName: UITextField!
    @IBOutlet var txtFieldUserID: UITextField!
    @IBOutlet var txtFieldLovedOneId: UITextField!
    var user:PFUser!
    
    override func viewDidLoad() {
        
        self.initObjects()
        
        //UI Setting
        self.addUIComponents();
        
        self.setDetails()
        //Gesture To End Editing
        
        //Add Back Button
        self.addBackButton();
        
        //Set Details
        self.setDetails()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = false
        self.navigationController?.navigationItem.hidesBackButton = false
        self.title = "Settings"
    }
    
    //MARK: Initial Setup
    func initObjects()
    {
        self.user = PFUser.currentUser()
    }
    
    func addUIComponents()
    {
        self.txtFieldUserID.layer.borderWidth = 0.5
        self.txtFieldUserID.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldUserID.textColor = UIColor.darkGrayColor()
        self.txtFieldUserID.delegate = self
        self.txtFieldUserID.returnKeyType = UIReturnKeyType.Next
        self.txtFieldUserID.userInteractionEnabled = false; //FIXME TEMPORY USER INTERACTION OFF
        
        self.txtFieldLovedOneId.layer.borderWidth = 0.5
        self.txtFieldLovedOneId.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldLovedOneId.textColor = UIColor.darkGrayColor()
        self.txtFieldLovedOneId.delegate = self
        self.txtFieldLovedOneId.returnKeyType = UIReturnKeyType.Done
        self.txtFieldLovedOneId.userInteractionEnabled = false;
        
        self.txtFieldUserLovedOneName.layer.borderWidth = 0.5
        self.txtFieldUserLovedOneName.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldUserLovedOneName.textColor = UIColor.darkGrayColor()
        self.txtFieldUserLovedOneName.delegate = self
        self.txtFieldUserLovedOneName.returnKeyType = UIReturnKeyType.Done
        self.txtFieldUserLovedOneName.userInteractionEnabled = false;

        
    }
    func setDetails()
    {
        let strName: String = self.user.objectForKey("name") as! String
        self.txtFieldUserID.text = "User Id: " + self.user.email!
        self.txtFieldLovedOneId.text = "Loved One Id: " + UserDefault.getUsersResidentId()
        self.txtFieldUserLovedOneName.text = "Loved One Name: " + strName
    }
    
    //MARK: Logical Function Action
    func addBackButton()
    {
        let image = UIImage(named:"imgMyPlateMenu")
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonItemClicked:"),animated: true)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGrayColor()
    }
    
    
    func barButtonItemClicked(sender: AnyObject)
    {
        let revealController:SWRevealViewController = self.revealViewController();
        revealController.revealToggle(sender)
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }

}
