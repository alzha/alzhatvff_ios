//
//  SubscribePublicVideosDetailsViewController.swift
//  AlzhaTV
//
//  Created by QMCPL on 14/04/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit
import Parse
import MobileCoreServices
import CoreFoundation
import MediaPlayer
import AVFoundation


extension NSTimeInterval {
    var minuteSecondMS: String {
        return String(format:"%d:%02d", minute, second)
    }
    var minute: Int {
        return Int(self/60.0 % 60)
    }
    var second: Int {
        return Int(self % 60)
    }
    var millisecond: Int {
        return Int(self*1000 % 1000 )
    }
}

extension Int {
    var msToSeconds: Double {
        return Double(self) / 1000
    }
}
class SubscribePublicVideosDetailsViewController: UIViewController,UICollectionViewDelegateFlowLayout, UIAlertViewDelegate
{
    @IBOutlet var collectionView : UICollectionView!
    
    var activityIndicator:UIActivityIndicatorView!
    var user:PFUser!
    var arrayPublicMedia :NSMutableArray = []
    var str_categoryName: String!
    var subscribePublicVideoDetails:PFObject!
    var objectMediaDetail : PFObject!
    
    let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.user = PFUser.currentUser()
        
        // Do any additional setup after loading the view.
        //print("subscribePublicVideoDetails \(self.subscribePublicVideoDetails)")
        self.str_categoryName = subscribePublicVideoDetails["name"] as! String
        //print("str_categoryName \(str_categoryName)")
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBar.hidden = false
        self.navigationController?.navigationItem.hidesBackButton = false
        self.title = "Get public video"
        self.getData()
    }
    
    func getData()
    {
        self.arrayPublicMedia.removeAllObjects()
        self.collectionView.reloadData()
        
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false
        
        let query = PFQuery(className:"PublicMedia")
        query.whereKey("category", equalTo:self.str_categoryName)
        query.findObjectsInBackgroundWithBlock{
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                if let objects = objects as? [PFObject]
                {
                    for object in objects
                    {
                        //print("++++ \(object.createdAt)")
                        self.arrayPublicMedia.addObject(object)
                    }
                }
                
                if (self.arrayPublicMedia.count == 0)
                {
                    self.removeActivityIndicator()
                    self.view.userInteractionEnabled = true
                    
                    if (objc_getClass("UIAlertController") != nil)
                    {
                        if #available(iOS 8.0, *) {
                            let alert = UIAlertController(title: "Alert", message: "No video found.", preferredStyle: UIAlertControllerStyle.Alert)

                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (alert) -> Void in
                                
                              self.navigationController?.popToRootViewControllerAnimated(true)
                            }))
                            self.presentViewController(alert, animated: true, completion: nil)
                        } else {
                            // Fallback on earlier versions
                        }
                       
                    }
                    else
                    {
                        let alert = UIAlertView(title: "Alert", message: "No video found.", delegate: nil , cancelButtonTitle:"Ok")
                        alert.show()
                    }
                }
                else
                {
                    self.removeActivityIndicator()
                    self.view.userInteractionEnabled = true
                    
                    self.collectionView.reloadData()
                }
            }
            else
            {
                // There was a problem, check error.description
                // Log details of the failure
                print("Error: \(error) \(error!.userInfo)")
                
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                
                //This will get error message sent by Parse SDK.
                let errorObj: AnyObject = error!.userInfo
                let errorString: String = errorObj.valueForKey("error") as! String
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                   
                }
                else
                {
                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
    }
    
    // MARK: UICollectionViewDataSource
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        return sectionInsets
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return self.arrayPublicMedia.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)as! ManageVideosCollectionViewCell
        
        let mediaDetailObject : PFObject = self.arrayPublicMedia[indexPath.row] as! PFObject
        print("mediaDetailObject \(mediaDetailObject)")
        
        cell.imgName.text = mediaDetailObject["name"] as? String
        cell.imgName.font = UIFont(name: "Arial-BoldMT", size: 14.0)
        cell.imgName.textAlignment = NSTextAlignment.Center
        cell.imgName.textColor = UIColor.darkGrayColor()
        
        cell.lblVideoDuration.textColor = UIColor.whiteColor()
        // cell.lblVideoDuration.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.5)
        
        cell.lblVideoDuration.backgroundColor = UIColor(white: 0, alpha: 0.5)
        let mins = Int((mediaDetailObject["duration"]?.description)!)!
        
        let time = mins.msToSeconds.minuteSecondMS
        
        cell.lblVideoDuration.text = "\(time)"
        cell.lblVideoDuration.font = UIFont(name: "Arial-BoldMT", size: 14.0)
        
        var image:UIImage!
        let userImageFile = mediaDetailObject["thumbNail"] as! PFFile
        userImageFile.getDataInBackgroundWithBlock {
            (imageData: NSData?, error: NSError?) -> Void in
            if error == nil
            {
                if let cellToUpdate = collectionView.cellForItemAtIndexPath(indexPath) as? ManageVideosCollectionViewCell
                {
                    
                    image = UIImage(data:imageData!)
                    //cellToUpdate.imageView.image = image as UIImage!
                   // print("image Size \(image.size)")
                    
                    
                    let imageRatio = image.size.width / image.size.height;
                    
                    let viewRatio = cellToUpdate.imageView.frame.size.width / cellToUpdate.imageView.frame.size.height;
                    
                    var imgsize = CGRectZero
                    
                    if(imageRatio < viewRatio)
                    {
                        let scale = cellToUpdate.imageView.frame.size.height / image.size.height;
                        
                        let width = scale * image.size.width;
                        
                        let topLeftX = (cellToUpdate.imageView.frame.size.width - width) * 0.5;
                        
                        imgsize  = CGRectMake(topLeftX, 0, width, cellToUpdate.imageView.frame.size.height);
                    }
                    else
                    {
                        let scale = cellToUpdate.imageView.frame.size.width / image.size.width;
                        
                        let height = scale * image.size.height;
                        
                        let topLeftY = (cellToUpdate.imageView.frame.size.height - height) * 0.5;
                        
                        imgsize = CGRectMake(0, topLeftY, cellToUpdate.imageView.frame.size.width, height);
                    }
                    
                   // print("imgsize \(imgsize)")
                    
                    let borderWidth: CGFloat = 1.0
                    let cornerRadius:CGFloat = 0.0
                    
                    // Create a multiplier to scale up the corner radius and border
                    // width you decided on relative to the imageViewer frame such
                    // that the corner radius and border width can be converted to
                    // the UIImage's scale.
                    let multiplier:CGFloat = image.size.height/cellToUpdate.imageView.frame.size.height > image.size.width/cellToUpdate.imageView.frame.size.width ?
                        image.size.height/cellToUpdate.imageView.frame.size.height :
                        image.size.width/cellToUpdate.imageView.frame.size.width
                    
                    let borderWidthMultiplied:CGFloat = borderWidth * multiplier
                    let cornerRadiusMultiplied:CGFloat = cornerRadius * multiplier
                    
                    UIGraphicsBeginImageContextWithOptions(image.size, false, 0)
                    
                    let path = UIBezierPath(roundedRect: CGRectInset(CGRectMake(0, 0, image.size.width, image.size.height),
                        borderWidthMultiplied / 2, borderWidthMultiplied / 2), cornerRadius: cornerRadiusMultiplied)
                    
                    let context = UIGraphicsGetCurrentContext()
                    
                    CGContextSaveGState(context)
                    // Clip the drawing area to the path
                    path.addClip()
                    
                    // Draw the image into the context
                    image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
                    CGContextRestoreGState(context)
                    
                    // Configure the stroke
                    UIColor.blackColor().setStroke()
                    path.lineWidth = borderWidthMultiplied
                    
                    // Stroke the border
                    path.stroke()
                    
                    cellToUpdate.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
                    
                    UIGraphicsEndImageContext();
                    
                    //cell.lblVideoDuration.frame = CGRectMake(0, 0, 42, 21)
                    var rect = cellToUpdate.lblVideoDuration.frame
                    //print("before rect \(rect)")
                    rect.origin.x = (imgsize.width + imgsize.origin.x) - rect.size.width + 10
                    rect.origin.y = (imgsize.height + imgsize.origin.y) - rect.size.height + 10
                    
                    //print("after rect \(rect)")
                    
                    cellToUpdate.lblVideoDuration.frame = rect
                    cellToUpdate.setNeedsLayout()
                }
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        self.objectMediaDetail = self.arrayPublicMedia[indexPath.row] as! PFObject
        print("self.objectMediaDetail +++ \(self.objectMediaDetail)")
        
        if (objc_getClass("UIAlertController") != nil)
        {
            if #available(iOS 8.0, *) {
                let alert: UIAlertController = UIAlertController(title: "", message: "Do you want to subscribe this video?", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler:{ action in
                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
                    })
                }))
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{ action in
                    self.checkForDuplicateEntry()
                    self.dismissViewControllerAnimated(true, completion:{ () -> Void in
                    })
                }))
                self.presentViewController(alert, animated: true, completion: { () -> Void in
                })
            } else {
                // Fallback on earlier versions
            }
                   }
        else
        {
            let alert = UIAlertView(title: "", message: "Do you want to subscribe this video?", delegate: self , cancelButtonTitle:"No", otherButtonTitles:"Yes")
            alert.show()
        }
    }
    
    //MARK:- Check if video is already subscribed.
    func checkForDuplicateEntry()
    {
        self.view.userInteractionEnabled = false
        self.showActivityIndicator()
        
        let query = PFQuery(className:"MediaDetails")
        query.whereKey("keyS3", equalTo:self.objectMediaDetail["keyS3"]!)
        query.whereKey("username", equalTo:self.user)
        query.findObjectsInBackgroundWithBlock{
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                
                if (objects!.count == 0)
                {
                    self.view.userInteractionEnabled = true
                    self.removeActivityIndicator()
                    self.uploadData()
                }
                else
                {
                    self.view.userInteractionEnabled = true
                    self.removeActivityIndicator()
                    
                    if (objc_getClass("UIAlertController") != nil)
                    {
                        if #available(iOS 8.0, *) {
                            let alert = UIAlertController(title: "Alert", message: "You have already subscribed for this video.", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
                        } else {
                            // Fallback on earlier versions
                        }
                       
                    }
                    else
                    {
                        let alert = UIAlertView(title: "Alert", message: "You have already subscribed for this video.", delegate: nil , cancelButtonTitle:"Ok")
                        alert.show()
                    }
                }
            }
            else
            {
                // Log details of the failure
                print("Error: \(error) \(error!.userInfo)")
                // There was a problem, check error.description
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                
                //This will get error message sent by Parse SDK.
                let errorObj: AnyObject = error!.userInfo
                let errorString: String = errorObj.valueForKey("error") as! String
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)

                    } else {
                        // Fallback on earlier versions
                    }
                                    }
                else
                {
                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
    }
    
    //MARK:- Upload data to Parse "Media Details"
    func uploadData()
    {
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false
        
        //-----------------------------
        let mediaDetails = PFObject(className:"MediaDetails")
        mediaDetails["keyS3"] = self.objectMediaDetail["keyS3"]
        mediaDetails["residentId"] = UserDefault.getUsersResidentId()
        mediaDetails["thumbNail"] = self.objectMediaDetail["thumbNail"]
        mediaDetails["dailyUpload"] = false
        mediaDetails["display"] = true
        mediaDetails["frequency"] = "Auto"
        mediaDetails["username"] = self.user
        mediaDetails["fileName"] = self.objectMediaDetail["name"]
        mediaDetails.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success)
            {
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                self.showToast("Subscribed Successfully...", view: self.view)
            }
            else
            {
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                // There was a problem, check error.description
                //This will get error message sent by Parse SDK.
                let errorObj: AnyObject = error!.userInfo
                let errorString: String = errorObj.valueForKey("error") as! String
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)

                    } else {
                        // Fallback on earlier versions
                    }
                                  }
                else
                {
                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
        //-----------------------------
    }
    
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }
    
    //MARK:- Show Toast after successful subscription
    func showToast(title:String, view:UIView)
    {
        let label = UILabel(frame: CGRectZero)
        label.textAlignment = NSTextAlignment.Center
        label.text = title
        label.font = UIFont(name: "Arial-Bold", size: 16.0)
        label.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)
        label.textColor = UIColor.whiteColor()
        label.sizeToFit()
        label.numberOfLines = 4
        label.layer.shadowColor = UIColor.grayColor().CGColor
        label.layer.shadowOffset = CGSizeMake(4, 3)
        label.layer.shadowOpacity = 0.3
        label.frame = CGRectMake(0, 44, 320, 44)
        view.addSubview(label)
        
        label.alpha = 0
        UIView.animateWithDuration(2.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            label.alpha = 1
            },  completion: {
                (value: Bool) in
                UIView.animateWithDuration(2.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                    label.alpha = 0
                    },  completion: {
                        (value: Bool) in
                        label.removeFromSuperview()
                })
        })
    }
    
    //MARK:- UIAlertViewDelegate Method
    func alertView(View: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex
        {
        case 0:
            print("0 case")
             self.navigationController?.popToRootViewControllerAnimated(true)

        case 1:
            self.checkForDuplicateEntry()
            
        default:
            print("Default case")
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }

}
