//
//  ManageVideosCollectionViewCell.swift
//  AlzhaTV
//
//  Created by QMCPL on 09/04/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit

class ManageVideosCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgName: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var btnCheckBox:UIButton!
    @IBOutlet var lblVideoDuration:UILabel!
    
    @IBOutlet var btnViewDetails: UIButton!
}
