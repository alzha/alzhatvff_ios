//
//  MenuViewController.swift
//  AlzhaTV
//
//  Created by QMCPL on 07/04/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit
import Parse

@objc
protocol MenuViewControllerDelegate
{
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate
{
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var lbl_UserName: UILabel!
    @IBOutlet var imgView_User:UIImageView!
    
    @IBOutlet weak var lbl_UserEmail: UILabel!
    @IBOutlet var image_Icon: UIImageView!
    //@IBOutlet var lbl_bg_imgUser:UIView!
    
    var delegate: MenuViewControllerDelegate?
    var arrayMenuItems: [String] = ["Make a video", "Upload videos", "Manage videos","Get public video","Customer Service","Invite family & friends","Settings","Logout"];
    var arrayMenuIcons: [String] = ["home", "upload", "newManage",
        "subscribe","customerservice_icon","invite","manage","logout"]
    var previousindex = 0
    var user: PFUser!
    var isiPhone4 : Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clearColor()
        self.menuTableView.alwaysBounceVertical = false
        
        // Do any additional setup after loading the view.
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        self.menuTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.menuTableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);
      //  self.menuTableView.reloadData()
        
       // deviceType : String = UIDevice.currentDevice.model;
        
        self.user = PFUser.currentUser()
        print("user.username \(self.user.username)")
        
        self.addUIComponents()
       /* if (menuTableView.contentSize.height < menuTableView.frame.size.height) {
            menuTableView.scrollEnabled = false
        }
        else {
            menuTableView.scrollEnabled = true
        }*/
        
    }

    
    func addUIComponents()
    {
        
        let strName: String = self.user.objectForKey("name") as! String
        //self.lbl_UserName.text = " Welcome" + " \(self.user.username)"
        self.lbl_UserName.text = strName
        
        self.lbl_UserEmail.text = self.user.email
        self.imgView_User.image = UIImage(named: "homeBackground")
        self.image_Icon.image = UIImage(named: "logo")
        
        
        
    }
    

    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 44;
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return self.arrayMenuItems.count
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        /*
        let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.AnimalCell, forIndexPath: indexPath) as AnimalCell
        cell.configureForAnimal(animals[indexPath.row])
        return cell
        */
        var separatorLine : UIImageView;
        let cellIdentifier = "cellIdentifier"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        if (cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            
            separatorLine = UIImageView(frame: CGRectMake(0.0, cell!.frame.size.height - 1.0, cell!.frame.size.width, 1.0))
            
            separatorLine.image = UIImage(named: "tempCellSeparator")
            separatorLine.image = separatorLine.image?.stretchableImageWithLeftCapWidth(1, topCapHeight: 0)
            separatorLine.tag = 4;
            cell!.contentView.addSubview(separatorLine)
        }else{
        
        separatorLine = cell?.viewWithTag(4) as! UIImageView

            
        }
        
        cell?.textLabel?.hidden = false;
        cell?.imageView?.hidden = false;
        
          separatorLine.hidden = true;
        
//        if(indexPath.row == 6)
//        {
//            cell?.textLabel?.hidden = true;
//            cell?.imageView?.hidden = true;
//        }else
//        {
            cell?.textLabel?.text = self.arrayMenuItems[indexPath.row]
            cell?.textLabel?.numberOfLines = 2
            cell?.textLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 14)
            cell?.textLabel?.textColor = UIColor.darkGrayColor()
            
            cell?.imageView?.image = UIImage(named :self.arrayMenuIcons[indexPath.row])
        //}
        
        /*if(indexPath.row > 5)
        {
            separatorLine.hidden = false;
        }*/
        
        
        return cell!
    }
    
    //Mark:- TableViewDelegateMethods
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let revealController:SWRevealViewController = self.revealViewController();
        
        if(indexPath.row == previousindex && indexPath.row != 4)
        {
            revealController.setFrontViewPositionLeft()
            return
        }
        
        if(indexPath.row == 0)
        {
            let storyboard:UIStoryboard = self.storyboard!
            let view:RecordVideoViewController = storyboard.instantiateViewControllerWithIdentifier("RecordVideoViewController") as! RecordVideoViewController
            //self.presentViewController(view, animated: true, completion: nil)
           revealController.pushFrontViewController(UINavigationController(rootViewController: view), animated: true)
            previousindex = indexPath.row
        }
        else if(indexPath.row == 1)
        {
            let storyboard:UIStoryboard = self.storyboard!
            let uploadview:SelectVideosViewController = storyboard.instantiateViewControllerWithIdentifier("SelectVideosViewController") as! SelectVideosViewController
            revealController.pushFrontViewController(UINavigationController(rootViewController: uploadview), animated: true)
            previousindex = indexPath.row
        }
        else if(indexPath.row == 2)
        {
            let storyboard:UIStoryboard = self.storyboard!
            let uploadview:ManageVideosViewController = storyboard.instantiateViewControllerWithIdentifier("ManageVideosViewController") as! ManageVideosViewController
            revealController.pushFrontViewController(UINavigationController(rootViewController: uploadview), animated: true)
            previousindex = indexPath.row
        }
        else if(indexPath.row == 3)
        {
            let storyboard:UIStoryboard = self.storyboard!
            let uploadview:SubscribePublicVideosViewController = storyboard.instantiateViewControllerWithIdentifier("SubscribePublicVideosViewController") as! SubscribePublicVideosViewController
            revealController.pushFrontViewController(UINavigationController(rootViewController: uploadview), animated: true)
            previousindex = indexPath.row
        }
            //FIXME IMPLEMENT FOR TESTING
        else if(indexPath.row == 5)
        {
            //Invite Friends
            let storyboard:UIStoryboard = self.storyboard!
            let inviteFriendFamilyVC:InviteTableViewController = storyboard.instantiateViewControllerWithIdentifier("InviteTableViewController") as! InviteTableViewController
            revealController.pushFrontViewController(UINavigationController(rootViewController: inviteFriendFamilyVC), animated: true)
            previousindex = indexPath.row
        }
        else if(indexPath.row == 7)
        {
            //Logout
            //-----
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert: UIAlertController = UIAlertController(title: "Logout?", message: "Do you want to logout?", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler:{ action in
                        self.dismissViewControllerAnimated(true, completion: { () -> Void in
                        })
                    }))
                    alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{ action in
                        self.logout()
                        self.dismissViewControllerAnimated(true, completion:{ () -> Void in
                        })
                    }))
                    self.presentViewController(alert, animated: true, completion: { () -> Void in
                    })
                } else {
                    // Fallback on earlier versions
                }
               
            }
            else
            {
                let alert = UIAlertView(title: "Logout?", message: "Do you want to logout?", delegate: self , cancelButtonTitle:"No", otherButtonTitles:"Yes")
                alert.show()
            }
        }
        else if(indexPath.row == 6)
        {
            //Settings
            let storyboard:UIStoryboard = self.storyboard!
            let manageIdVC:ManageIdViewController = storyboard.instantiateViewControllerWithIdentifier("ManageIdViewController") as! ManageIdViewController
            revealController.pushFrontViewController(UINavigationController(rootViewController: manageIdVC), animated: true)
            previousindex = indexPath.row
        }
        else if(indexPath.row == 4)
        {
            //Settings
            let storyboard:UIStoryboard = self.storyboard!
            let customerService:CustomerServicesViewController = storyboard.instantiateViewControllerWithIdentifier("CustomerServices") as! CustomerServicesViewController
            revealController.pushFrontViewController(UINavigationController(rootViewController: customerService), animated: true)
            previousindex = indexPath.row
        }
    }
    
    func logout()
    {
        PFUser.logOut()
        UserDefault.setUsersResidentId("")
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let storyboard:UIStoryboard = self.storyboard!
        
        let loginViewController:LoginViewController = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        
        appDelegate.window!.rootViewController = UINavigationController(rootViewController: loginViewController)
        appDelegate.window!.makeKeyAndVisible()
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isInfomanage")
         NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isInfoPublic")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    //MARK:- UIAlertViewDelegate Method
    func alertView(View: UIAlertView,clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex
        {
        case 0:
            print("0 case")
            
        case 1:
            self.logout()
            //self.navigationController?.popToRootViewControllerAnimated(true)
            
        default:
            print("Default case")
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }

    override func shouldAutorotate() -> Bool {
        return false
    }
}
