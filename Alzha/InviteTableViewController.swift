//
//  InviteTableViewController.swift
//  AlzhaTV
//
//  Created by Yogesh on 19/06/15.
//  Copyright (c) 2015 QMC243-MAC-ShrikantP. All rights reserved.
//

import UIKit
import Parse

class InviteTableViewController: UITableViewController,UITextFieldDelegate,UIAlertViewDelegate {
    
    var arrayOfEmails : NSMutableArray = []
    var activityIndicator:UIActivityIndicatorView!
    var user:PFUser!
    var txtEmailfield : UITextField = UITextField()
    var indexPathToDelete:NSIndexPath?;
    
    override func viewDidLoad() {
        //Add Back Button
        self.initObjects()
        self.addBackButton()
        self.getData()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = false
        self.navigationController?.navigationItem.hidesBackButton = false
        self.title = "Invite family & friends"
        
    }
    
    //MARK: Initial Setup
    func addBackButton(
        
        )
    {
        let image = UIImage(named:"imgMyPlateMenu")
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonItemClicked:"),animated: true)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGrayColor()
    }
    
    //MARK: Logical Function & Action
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let range = testStr.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
        let result = range != nil ? true : false
        
        return result
    }
    
    
    func barButtonItemClicked(sender: AnyObject)
    {
         self.view.endEditing(true)
        let revealController:SWRevealViewController = self.revealViewController();
        revealController.revealToggle(sender)
    }
    
    func endEditing()
    {
        self.view.endEditing(true);
    }
    
    func initObjects()
    {
        self.user = PFUser.currentUser()
    }
    // MARK: Table View Data Source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayOfEmails.count + 1;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        /*
        let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.AnimalCell, forIndexPath: indexPath) as AnimalCell
        cell.configureForAnimal(animals[indexPath.row])
        return cell
        */
        let cellIdentifier = "cellIdentifier"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        var textFieldEmailId_ : UITextField = UITextField();
       
        let paddingView : UIView = UIView(frame: CGRectMake(0, 0, 5, 20))
        
        var btnAdd : UIButton = UIButton();
        
        if cell == nil
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            textFieldEmailId_.autoresizingMask = UIViewAutoresizing.FlexibleWidth
            textFieldEmailId_ = UITextField(frame: CGRectMake(10, 0, cell!.frame.size.width - 50, cell!.frame.size.height));
            textFieldEmailId_.placeholder = "Email address";
            textFieldEmailId_.layer.borderWidth = 0.5
            textFieldEmailId_.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
            textFieldEmailId_.tag = 101;
            textFieldEmailId_.delegate = self;
            textFieldEmailId_.autocapitalizationType =  UITextAutocapitalizationType.None;
            textFieldEmailId_.keyboardType = UIKeyboardType.EmailAddress
            
            textFieldEmailId_.leftView = paddingView;
            textFieldEmailId_.leftViewMode = UITextFieldViewMode.Always;
            
            btnAdd = UIButton(frame: CGRectMake(cell!.frame.size.width - 40, 0, 40, cell!.frame.size.height));
             btnAdd.autoresizingMask = UIViewAutoresizing.FlexibleWidth
            btnAdd.addTarget(self, action: "btnClickedInviteEmail:", forControlEvents:UIControlEvents.TouchUpInside);
            btnAdd.setTitle("Add", forState: UIControlState.Normal);
            btnAdd.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)
            btnAdd.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            btnAdd.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
            btnAdd.tag = 102;
            
            cell?.contentView.addSubview(textFieldEmailId_);
            
            cell?.contentView.addSubview(btnAdd);
             txtEmailfield = textFieldEmailId_
            
        }else
        {
            textFieldEmailId_ = (cell?.viewWithTag(101) as? UITextField)!;
            btnAdd = (cell?.viewWithTag(102) as? UIButton)!;
        }
        
        if(indexPath.row == 0)
        {
            textFieldEmailId_.hidden = false;
            btnAdd.hidden = false;
            
        }else if((indexPath.row > 0) && indexPath.row <= self.arrayOfEmails.count)
        {
            textFieldEmailId_.hidden = true;
            btnAdd.hidden = true;
            
            //print("++++ \(object.createdAt)")
            let inviationObject : PFObject = self.arrayOfEmails[indexPath.row - 1] as! PFObject
            
            let email = inviationObject["email"] as! String
            
            cell?.separatorInset = UIEdgeInsetsZero
            cell?.textLabel?.text = email
            cell?.textLabel?.numberOfLines = 1
            cell?.textLabel?.font = UIFont(name: "Arial", size: 14)
            cell?.textLabel?.textColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)
            //    cell?.indentationLevel = 2
            
        }
        
        
        return cell!
    }
    
    
    //Mark:- TableViewDelegateMethods
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true;
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete
        {
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert: UIAlertController = UIAlertController(title: "Delete?", message: "Do you want to remove this invitation?", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler:{ action in
                        self.dismissViewControllerAnimated(true, completion: { () -> Void in
                        })
                    }))
                    alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{ action in
                        self.deleteData(self.arrayOfEmails[indexPath.row - 1] as! PFObject )
                        self.dismissViewControllerAnimated(true, completion:{ () -> Void in
                        })
                    }))
                    self.presentViewController(alert, animated: true, completion: { () -> Void in
                    })
                    
                } else {
                    // Fallback on earlier versions
                }
                
            }
            else
            {
                let alert = UIAlertView(title: "Delete?", message: "Do you want to remove this invitation?", delegate: self , cancelButtonTitle:"No", otherButtonTitles:"Yes")
                alert.tag = 1001
                
                indexPathToDelete = indexPath
                alert.show()
            }
        }
    }
    
    //Mark: ALERTVIEW delegate
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.tag == 1001 && buttonIndex == 1
        {
            self.deleteData(self.arrayOfEmails[indexPathToDelete!.row - 1] as! PFObject );
        }
    }
    
    
    //Mark: Logical functions & actions
    func btnClickedInviteEmail(Sender :UIButton!)
    {
        //GETTING TEXTFIELD FROM FIRST CELL
        let cellIndexPath:NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
        let emailCell :UITableViewCell = self.tableView.cellForRowAtIndexPath(cellIndexPath)!
        let emailTxtFLd : UITextField = emailCell.viewWithTag(101) as! UITextField;
        
        
        NSLog("Email : \(emailTxtFLd.text)");
        //ADDING TO ARRAY
        if !emailTxtFLd.text!.isEmpty
        {
            if self.isValidEmail(emailTxtFLd.text!)
            {
                self.saveData(emailTxtFLd);
            }else
            {
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Alert", message: "Please enter valid email address!", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                    
                }
                else
                {
                    let alert = UIAlertView(title: "Alert", message: "Please enter valid email address!", delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
            
        }
        
    }
    
    //MARK: Parse Database Activities
    func deleteData( inviationDetails: PFObject)
    {
        self.showActivityIndicator()
        inviationDetails.deleteInBackgroundWithBlock{
            (success: Bool, error: NSError?) -> Void in
            if (success)
            {
                // The object has been saved.
                self.removeActivityIndicator()
                self.getData()
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "AlzhaTV", message: "Invitation Removed Successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:{ action in
                            self.dismissViewControllerAnimated(false, completion:{ () -> Void in
                            })
                            self.navigationController?.popViewControllerAnimated(true)
                        }))
                        self.presentViewController(alert, animated: true, completion: { () -> Void in
                        })
                    } else {
                        // Fallback on earlier versions
                    }
                   
                }
                else
                {
                    let alert = UIAlertView(title: "AlzhaTV", message: "Invitation Removed Successfully.", delegate: self , cancelButtonTitle:"Ok")
                    alert.tag = 100
                    alert.show()
                }
            }
            else
            {
                self.removeActivityIndicator()
                self.view.userInteractionEnabled = true
                
                //This will get error message sent by Parse SDK.
                let errorObj: AnyObject = error!.userInfo
                let errorString: String = errorObj.valueForKey("error") as! String
                
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    else {
                        // Fallback on earlier versions
                    }
                   
                }
                else
                {
                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
        
    }
    
    
    func saveData( emailTxtFLd: UITextField)
    {
        self.showActivityIndicator();
        
        //-----------------------------
        // Upload data in class "Invitation"
        let mediaDetails = PFObject(className:"Invitation")
        mediaDetails["residentId"] = UserDefault.getUsersResidentId()
        mediaDetails["email"] = emailTxtFLd.text!.lowercaseString
        mediaDetails["username"] = self.user
        
        mediaDetails.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success)
            {
                self.removeActivityIndicator()
                emailTxtFLd.text = "";
                self.getData()
                
            }
            else
            {
                
                // There was a problem, check error.description
                self.removeActivityIndicator()
                
                //This will get error message sent by Parse SDK.
                let errorObj: AnyObject = error!.userInfo
                let errorString: String = errorObj.valueForKey("error") as! String
                
                if (objc_getClass("UIAlertController") != nil)
                {
                   if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)

                    } else {
                        // Fallback on earlier versions
                    }
                    
                                   }
                else
                {
                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
        //-----------------------------
    }
    
    func getData()
    {
        
        let query = PFQuery(className:"Invitation")
        //query.whereKey("username", equalTo:self.user) //Suggested by Sharad 20/04/2015
        query.whereKey("residentId", equalTo:UserDefault.getUsersResidentId())
        query.orderByDescending("createdAt");
        query.findObjectsInBackgroundWithBlock{
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                // The find succeeded.
                
                if let objects = objects as? [PFObject]
                {
                    self.arrayOfEmails = []
                    for object in objects
                    {
                        
                        
                        self.arrayOfEmails.addObject(object)
                    }
                    self.tableView.reloadData()
                }
                
                if (self.arrayOfEmails.count == 0)
                {
                    self.removeActivityIndicator()
                    
                }
                else
                {
                    self.removeActivityIndicator()
                    
                }
            }
            else
            {
                
                self.removeActivityIndicator()
            }
        }
    }
    
    //MARK: UI Logics
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.tableView.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
        self.view.userInteractionEnabled = false;
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
        self.view.userInteractionEnabled = true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        self.view.endEditing(true);
        
        return true;
    }
    
//    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
//        return .Portrait
//    }

    override func shouldAutorotate() -> Bool {
        return false
    }
    
   
}//END
