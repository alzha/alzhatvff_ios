//
//  ResidentIdViewController.swift
//  AlzhaTV
//
//  Created by QMCPL on 02/04/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class ResidentIdViewController: UIViewController, UITextFieldDelegate, UIAlertViewDelegate
{
    @IBOutlet var txtFieldResidentId: UITextField!
    @IBOutlet var txtFieldName: UITextField!
    @IBOutlet var txtFieldHospitalId: UITextField!
    @IBOutlet var txtFieldHospitalRoomNo: UITextField!
    @IBOutlet var btnSubmitResidentId: UIButton!
    
    var activityIndicator:UIActivityIndicatorView!
    var isFirstTimeSetup : Bool = false
    var user:PFUser!
    var primary:Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.user = PFUser.currentUser()
        print("isFirstTimeSetup \(self.isFirstTimeSetup)")
        print("user.objectId \(self.user.objectId)")
        print("user.username \(self.user.username)")
        
        self.addUIComponents()
    }
    
    func addUIComponents()
    {
        self.btnSubmitResidentId.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)
        self.btnSubmitResidentId.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnSubmitResidentId.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
        self.btnSubmitResidentId.layer.cornerRadius = 10.0
        
        self.txtFieldResidentId.layer.borderWidth = 0.5
        self.txtFieldResidentId.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldResidentId.textColor = UIColor.darkGrayColor()
        self.txtFieldResidentId.delegate = self
        
        if self.isFirstTimeSetup
        {
            self.txtFieldResidentId.returnKeyType = UIReturnKeyType.Next
            self.btnSubmitResidentId.frame = CGRectMake(self.btnSubmitResidentId.frame.origin.x , self.btnSubmitResidentId.frame.origin.y, self.btnSubmitResidentId.frame.size.width, self.btnSubmitResidentId.frame.size.height)
        }
        else
        {
            self.txtFieldResidentId.returnKeyType = UIReturnKeyType.Done
            self.btnSubmitResidentId.frame = CGRectMake(self.btnSubmitResidentId.frame.origin.x , self.txtFieldResidentId.frame.origin.y + 40, self.btnSubmitResidentId.frame.size.width, self.btnSubmitResidentId.frame.size.height)
        }
        
        self.txtFieldName.layer.borderWidth = 0.5
        self.txtFieldName.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldName.textColor = UIColor.darkGrayColor()
        self.txtFieldName.delegate = self
        self.txtFieldName.returnKeyType = UIReturnKeyType.Next
        
        self.txtFieldHospitalId.layer.borderWidth = 0.5
        self.txtFieldHospitalId.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldHospitalId.textColor = UIColor.darkGrayColor()
        self.txtFieldHospitalId.delegate = self
        self.txtFieldHospitalId.returnKeyType = UIReturnKeyType.Next
        
        self.txtFieldHospitalRoomNo.layer.borderWidth = 0.5
        self.txtFieldHospitalRoomNo.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldHospitalRoomNo.textColor = UIColor.darkGrayColor()
        self.txtFieldHospitalRoomNo.delegate = self
        self.txtFieldHospitalRoomNo.returnKeyType = UIReturnKeyType.Done
        
        if(self.isFirstTimeSetup)
        {
            self.primary = true
            self.txtFieldResidentId.placeholder = "Enter Resident Id to register"
            self.txtFieldName.alpha = 1
            self.txtFieldHospitalId.alpha = 1
            self.txtFieldHospitalRoomNo.alpha = 1
        }
        else
        {
            self.primary = false
            self.txtFieldResidentId.placeholder = "Enter your Resident Id"
            self.txtFieldName.alpha = 0
            self.txtFieldHospitalId.alpha = 0
            self.txtFieldHospitalRoomNo.alpha = 0
        }
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBar.hidden = false
        self.navigationController?.navigationItem.hidesBackButton = false
    }
    
    @IBAction func btnSubmitResidentIdClicked(sender: AnyObject)
    {
        let result = self.checkFieldsComplete()
        
        if(result == "" || result.isEmpty)
        {
            self.registerResidentId()
        }
        else
        {
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Alert", message: result, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
               
            }
            else
            {
                let alert = UIAlertView(title: "Alert", message: result, delegate: nil , cancelButtonTitle:"Ok")
                alert.show()
            }
        }
    }
    
    //MARK:- Checking for empty textfields
    func checkFieldsComplete() ->String
    {
        if(self.isFirstTimeSetup)
        {
            if(self.txtFieldResidentId.text == "" || self.txtFieldResidentId.text!.isEmpty || self.txtFieldName.text == "" || self.txtFieldName.text!.isEmpty || self.txtFieldHospitalId.text == "" || self.txtFieldHospitalId.text!.isEmpty || self.txtFieldHospitalRoomNo.text == "" || self.txtFieldHospitalRoomNo.text!.isEmpty)
            {
                return "You need to complete all fields."
            }
            else
            {
                return ""
            }
        }
        else
        {
            if(self.txtFieldResidentId.text == "" || self.txtFieldResidentId.text!.isEmpty )
            {
                return "Enter your resident id."
            }
            else
            {
                return ""
            }
        }
    }
    
    func pushRecordView()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let storyboard:UIStoryboard = self.storyboard!
        
        let menuViewController:MenuViewController = storyboard.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
        
        let view:RecordVideoViewController = storyboard.instantiateViewControllerWithIdentifier("RecordVideoViewController") as! RecordVideoViewController
        
        let revealController:SWRevealViewController  = SWRevealViewController()
        revealController.rearViewController = menuViewController
        revealController.frontViewController = UINavigationController(rootViewController: view)
        revealController.delegate = appDelegate;
        
        appDelegate.window!.rootViewController = revealController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }
    
    //MARK:- Register Resident Id to Parse
    func registerResidentId()
    {
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false
        
        let query = PFQuery(className:"Resident")
        query.whereKey("residentId", equalTo:self.txtFieldResidentId.text!)
        query.whereKey("primary", equalTo:true)
        //query.whereKey("username", equalTo:self.user) //Suggested by Sharad 20/04/2015
        query.findObjectsInBackgroundWithBlock{
            (mainobjects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                // The find succeeded.
                print("Successfully retrieved \(mainobjects!.count) scores.")
                if (mainobjects!.count == 0)
                {
                    if (!self.isFirstTimeSetup)
                    {
                        self.view.userInteractionEnabled = true
                        self.removeActivityIndicator()
                        
                        print("Not First Time, count == 0")
                        print("isFirstTimeSetup1 \(self.isFirstTimeSetup)")
                        
                        if (objc_getClass("UIAlertController") != nil)
                        {
                           if #available(iOS 8.0, *) {
                                let alert = UIAlertController(title: "Alert", message: "No Resident Id exists for this user. Please enter Resident Id to register.", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:{ action in
                                self.dismissViewControllerAnimated(false, completion:{ () -> Void in
                                })
                                self.navigationController?.popViewControllerAnimated(true)
                            }))
                            self.presentViewController(alert, animated: true, completion: { () -> Void in
                            })
                            }
                           else {
                                // Fallback on earlier versions
                            }
                            
                        }
                        else
                        {
                            let alert = UIAlertView(title: "Alert", message: "No Resident Id exists for this user. Please enter Resident Id to register.", delegate: self , cancelButtonTitle:"Ok")
                            alert.show()
                        }
                    }
                    else
                    {
                        self.view.userInteractionEnabled = true
                        self.removeActivityIndicator()
                        
                        //MARK:- create resident id, save in DB
                        print("First Time, count == 0")
                        print("isFirstTimeSetup2 \(self.isFirstTimeSetup)")
                        
                        let resident = PFObject(className:"Resident")
                        resident["residentId"] = self.txtFieldResidentId.text
                        resident["username"] = self.user
                        resident["name"] = self.txtFieldName.text
                        resident["hospitalId"] = self.txtFieldHospitalId.text
                        resident["hospitalRoomNo"] = self.txtFieldHospitalRoomNo.text
                        resident["primary"] = true
                        resident.saveInBackgroundWithBlock {
                            (success: Bool, error: NSError?) -> Void in
                            if (success)
                            {
                                self.view.userInteractionEnabled = true
                                self.removeActivityIndicator()
                                
                                UserDefault.setUsersResidentId(self.txtFieldResidentId.text!)
                                NSUserDefaults.standardUserDefaults().synchronize()
                                
                                self.pushRecordView()
                            }
                            else
                            {
                                // There was a problem, check error.description
                                self.removeActivityIndicator()
                                self.view.userInteractionEnabled = true
                                
                                //This will get error message sent by Parse SDK.
                                let errorObj: AnyObject = error!.userInfo
                                let errorString: String = errorObj.valueForKey("error") as! String
                                
                                if (objc_getClass("UIAlertController") != nil)
                                {
                                    if #available(iOS 8.0, *) {
                                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                                        self.presentViewController(alert, animated: true, completion: nil)
                                    } else {
                                        // Fallback on earlier versions
                                    }
                                    
                                }
                                else
                                {
                                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                                    alert.show()
                                }
                            }
                        }
                    }
                }
                else
                {
                    if(!self.isFirstTimeSetup)
                    {
                        var residentObject : PFObject!
                        if let obj = mainobjects as? [PFObject]
                        {
                            residentObject = obj[0] as PFObject
                        }
                        
                        //
                        let resident = PFObject(className:"Resident")
                        resident["residentId"] = self.txtFieldResidentId.text
                        resident["username"] = self.user
                        resident["name"] = residentObject["name"]
                        resident["hospitalId"] = residentObject["hospitalId"]
                        resident["hospitalRoomNo"] = residentObject["hospitalRoomNo"]
                        resident["primary"] = false
                        resident.saveInBackgroundWithBlock {
                            (success: Bool, error: NSError?) -> Void in
                            if (success)
                            {
                                self.view.userInteractionEnabled = true
                                self.removeActivityIndicator()
                                UserDefault.setUsersResidentId(self.txtFieldResidentId.text!)
                                NSUserDefaults.standardUserDefaults().synchronize()
                                
                                self.pushRecordView()
                            }
                            else
                            {
                                // There was a problem, check error.description
                                self.removeActivityIndicator()
                                self.view.userInteractionEnabled = true
                                
                                //This will get error message sent by Parse SDK.
                                let errorObj: AnyObject = error!.userInfo
                                let errorString: String = errorObj.valueForKey("error") as! String
                                
                                if (objc_getClass("UIAlertController") != nil)
                                {
                                    if #available(iOS 8.0, *) {
                                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                                        self.presentViewController(alert, animated: true, completion: nil)
                                    } else {
                                        // Fallback on earlier versions
                                    }
                                   
                                }
                                else
                                {
                                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                                    alert.show()
                                }
                            }
                        }

                    }
                        else
                    {
                        
                        self.removeActivityIndicator()
                        self.view.userInteractionEnabled = true
                        
                        if (objc_getClass("UIAlertController") != nil)
                        {
                            if #available(iOS 8.0, *) {
                                let alert = UIAlertController(title: "Alert", message: "This id is not available.", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:{ action in
                                    self.dismissViewControllerAnimated(false, completion:{ () -> Void in
                                    })
                                    //self.navigationController?.popViewControllerAnimated(true)
                                }))
                                self.presentViewController(alert, animated: true, completion: { () -> Void in
                                })
                            } else {
                                // Fallback on earlier versions
                            }
                                                    }
                        else
                        {
                            let alert = UIAlertView(title: "Alert", message: "This id is not available.", delegate: nil , cancelButtonTitle:"Ok")
                            alert.show()
                        }
                    }
                }
            }
                
            
                else
                {
                    // Log details of the failure
                    print("Error: \(error) \(error!.userInfo)")
                    // There was a problem, check error.description
                    self.removeActivityIndicator()
                    self.view.userInteractionEnabled = true
                    
                    //This will get error message sent by Parse SDK.
                    let errorObj: AnyObject = error!.userInfo
                    let errorString: String = errorObj.valueForKey("error") as! String
                    
                    if (objc_getClass("UIAlertController") != nil)
                    {
                        if #available(iOS 8.0, *) {
                            let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
                        } else {
                            // Fallback on earlier versions
                        }
                        
                    }
                    else
                    {
                        let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                        alert.show()
                    }
                }
            
        
        }
        }
        
        
        
        
        
        
        func textFieldShouldReturn(textField: UITextField) -> Bool
        {
            //self.btnSubmitResidentIdClicked("")
            //return true
            if (self.isFirstTimeSetup)
            {
                if textField == self.txtFieldResidentId
                {
                    self.txtFieldName.becomeFirstResponder()
                }
                else if textField == self.txtFieldName
                {
                    self.txtFieldHospitalId.becomeFirstResponder()
                }
                else if textField == self.txtFieldHospitalId
                {
                    self.txtFieldHospitalRoomNo.becomeFirstResponder()
                }
                else
                {
                    self.btnSubmitResidentIdClicked("")
                    textField.resignFirstResponder()
                }
            }
            else
            {
                self.btnSubmitResidentIdClicked("")
            }
            return true
        }
        
        //MARK:- UIAlertViewDelegate Method
        func alertView(View: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
        {
            switch buttonIndex
            {
            case 0:
                self.navigationController?.popViewControllerAnimated(true)
                
            default:
                print("Default case")
            }
        }
        
        //MARK:- Back Button Action
        @IBAction func btnBackClicked(sender: AnyObject)
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
        
        override func didReceiveMemoryWarning()
        {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
}
