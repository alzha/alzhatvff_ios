//
//  ViewController.swift
//
//  Copyright 2011-present Parse Inc. All rights reserved.
//

import UIKit
import Parse
import MobileCoreServices
import CoreFoundation
import MediaPlayer
import AVFoundation

@objc
protocol ViewControllerDelegate
{
    optional func toggleLeftPanel()
    optional func toggleRightPanel()
    optional func collapseSidePanels()
}

class ViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    @IBOutlet var btnLogout: UIButton!
    @IBOutlet var btnRecordVideo: UIButton!
    @IBOutlet var imgViewThumbnail:UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet var statusLabel:UILabel!
    
    var isComingFromLogin: Bool = false
    var activityIndicator:UIActivityIndicatorView!
    var user:PFUser!
    var delegate: ViewControllerDelegate?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.user = PFUser.currentUser()
        
        self.btnRecordVideo.backgroundColor = UIColor.darkGrayColor()
        self.btnRecordVideo.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnRecordVideo.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
        
        self.btnLogout.backgroundColor = UIColor.redColor()
        self.btnLogout.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnLogout.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
        self.btnLogout.layer.cornerRadius = 10.0
        
        self.statusLabel.textColor = UIColor.purpleColor()
        self.statusLabel.textAlignment = NSTextAlignment.Center
        self.statusLabel.layer.cornerRadius = 10.0
        
        var image = UIImage(named:"imgMyPlateMenu")
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonItemClicked:"),animated: true)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBar.hidden = false
        self.navigationItem.hidesBackButton = true
        self.title = "Record Video"
    }
    
    func barButtonItemClicked(sender: AnyObject)
    {
        delegate?.toggleLeftPanel?()
    }
    
    //MARK:- Logout Button Action
    @IBAction func btnLogoutClicked(sender: AnyObject)
    {
        PFUser.logOut()
        UserDefault.setUsersResidentId("")
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    //MARK:- Record Button Action..directly connected from storyboard
    @IBAction func btnRecordVideoClicked(sender: AnyObject)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            println("captureVideoPressed and camera available.")
            var imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .Camera;
            imagePicker.mediaTypes = [kUTTypeMovie!]
            imagePicker.allowsEditing = false
            imagePicker.showsCameraControls = true
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
            println("Camera not available.")
            var alert = UIAlertController(title: "Oppss !!!", message: "Camera not available.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- ImagePickerController Delegate Methods
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info:NSDictionary!)
    {
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false
        
        //MARK:- Generate key for image
        var uuid = NSUUID().UUIDString
        println("uuid \(uuid)")
        
        //This will save recorded video to Album
        let tempImage = info[UIImagePickerControllerMediaURL] as NSURL!
        let pathString = tempImage.relativePath
        self.dismissViewControllerAnimated(true, completion: {})
        
        /*
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(pathString))
        {
            UISaveVideoAtPathToSavedPhotosAlbum(pathString, self,
                "image:didFinishSavingWithError:contextInfo:", nil)
        }
        */
        
        //This will save recorded video to Documents
        var videoUrl: NSURL = info.objectForKey(UIImagePickerControllerMediaURL) as NSURL
        var videoData: NSData = NSData(contentsOfURL: videoUrl)!
        var paths: NSArray = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        var documentsPath: NSString = paths.objectAtIndex(0) as NSString
        
        var videoNameStr : NSDate = NSDate()
        var dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
        var videoName: String = dateFormatter.stringFromDate(videoNameStr)
        var str_VideoName : String = "VID_\(videoName)"
        println("str_VideoName \(str_VideoName)")
        
        var docPath: NSString = documentsPath.stringByAppendingPathComponent("/\(str_VideoName).mp4")
        var success: Bool = videoData.writeToFile(docPath, atomically: false)
        println("docPath ---> \(docPath)")
        
        //This will check whether video exist in Docments or not.
        var exists: Bool = NSFileManager.defaultManager().fileExistsAtPath(docPath)
        if(exists)
        {
            println("Video exists")
        }
        else
        {
            println("video doesn't exist")
        }
        
        //MARK:- Generate thumbnail of a video.
        var asset:AVURLAsset = AVURLAsset(URL: videoUrl, options: nil)
        var gen:AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        gen.appliesPreferredTrackTransform = true
        var time:CMTime = CMTimeMakeWithSeconds(0,30)
        var error:NSError?
        var image:CGImageRef = gen.copyCGImageAtTime(time, actualTime:nil, error: &error)
        var thumb:UIImage = UIImage(CGImage: image)!
        self.imgViewThumbnail.image = thumb as UIImage!
        var imageData = UIImagePNGRepresentation(thumb)
        
        //MARK:- Generate and save thumbnail in .png fromat
        var pngPath: String = NSHomeDirectory().stringByAppendingPathComponent("Documents/thumbNail.png")
        UIImagePNGRepresentation(thumb).writeToFile(pngPath, atomically: true)
        
        // Create file manager
        var error1: NSError?
        var fileMgr:NSFileManager = NSFileManager.defaultManager()
        
        // Point to Document directory
        var docDir  :String = NSHomeDirectory().stringByAppendingPathComponent("Documents")
        
        // Write out the contents of home directory to console
        println("Documents directory: \(fileMgr.contentsOfDirectoryAtPath(docDir, error: &error1))")
        
        /* //This will also generate thumbnail of a video.
        let asset1 = AVURLAsset(URL:videoUrl, options:nil)
        let generator = AVAssetImageGenerator(asset: asset1)
        let time = CMTimeMakeWithSeconds(0, 30)
        let size = CGSizeMake(425,355)
        generator.maximumSize = size
        let imgRef = generator.copyCGImageAtTime(time, actualTime: nil, error: nil)
        let thumb = UIImage(CGImage:imgRef)
        
        println("thumb \(thumb)")
        self.imgViewThumbnail.image = thumb as UIImage!
        */
        
        /////////=======================
        //MARK:- Upload video to Amazon server.
        let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "us-east-1:631186b9-404c-4341-91f3-230e686b5993")
        let configuration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: credentialProvider)
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = configuration
        
        var uploadFileURL = NSURL.fileURLWithPath(docPath)
        
        var uploadRequest = AWSS3TransferManagerUploadRequest();
        uploadRequest.bucket = "alzha";
        uploadRequest.key = uuid + ".mp4";
        println("uploadRequest.key \(uploadRequest.key)")
        uploadRequest.body = uploadFileURL;
        
        uploadRequest.uploadProgress = { (bytesSent:Int64, totalBytesSent:Int64,  totalBytesExpectedToSend:Int64) -> Void in
            let progress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
            dispatch_sync(dispatch_get_main_queue(), {() -> Void in
                self.progressView.progress = progress
                self.statusLabel.text = "Uploading..."
                println("totalBytesSent \(totalBytesSent)")
            })
        }
        
        let S3TransferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        S3TransferManager.upload(uploadRequest).continueWithExecutor(BFExecutor.mainThreadExecutor(), withBlock:{
            (task: BFTask!) -> BFTask! in
            
            if(task.error != nil)
            {
                var errorObj: AnyObject = task.error.userInfo!
                println("error=\(errorObj)")
            }
            else
            {
                println("upload succesfully")
                
                //MARK:- Upload data in class "Media Details"
                let imageFile = PFFile(name:"thumbNail.png", data:imageData)
                var mediaDetails = PFObject(className:"MediaDetails")
                mediaDetails["residentId"] = UserDefault.getUsersResidentId()
                mediaDetails["thumbNail"] = imageFile
                mediaDetails["dailyUpload"] = true
                mediaDetails["keyS3"] = uuid
                mediaDetails["username"] = self.user
                mediaDetails["fileName"] = docPath
                mediaDetails.saveInBackgroundWithBlock {
                    (success: Bool, error: NSError!) -> Void in
                    if (success)
                    {
                        self.removeActivityIndicator()
                        self.view.userInteractionEnabled = true
                        
                        var alert = UIAlertController(title: "Great !!!", message: "Record Added Successfully", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                         self.statusLabel.text = "Uploaded Successfully..."
                    }
                    else
                    {
                        // There was a problem, check error.description
                        self.removeActivityIndicator()
                        self.view.userInteractionEnabled = true
                        
                        //This will get error message sent by Parse SDK.
                        var errorObj: AnyObject = error.userInfo!
                        var errorString: String = errorObj.valueForKey("error") as String
                        
                        var alert = UIAlertController(title: "Error !!!", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                
            }
            
            return nil
        })
        ////////========================
    }
    
    func image(image: UIImage, didFinishSavingWithError
        error: NSErrorPointer, contextInfo:UnsafePointer<Void>)
    {
        if error != nil
        {
            var alert = UIAlertController(title: "Error !!!", message: "Video saving failed.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            self.showActivityIndicator()
            self.view.userInteractionEnabled = false
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func uploadVideoToServer(testStr:String)
    {
        
    }
    
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

