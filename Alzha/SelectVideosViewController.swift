//
//  SelectVideosViewController.swift
//  AlzhaTV
//
//  Created by QMCPL on 08/04/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit
import Parse
import MobileCoreServices
import CoreFoundation
import MediaPlayer
import AVFoundation
import Photos
import AssetsLibrary


class SelectVideosViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate
{
    @IBOutlet var lblPercentage: UILabel!
    @IBOutlet var btnSelectVideo: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet var statusLabel:UILabel!
    @IBOutlet var btnSelectMultipleVideo:UIButton!
    @IBOutlet var imgviewThumbnail:UIImageView!
    
    var activityIndicator:UIActivityIndicatorView!
    var user:PFUser!
    var progressVal : Float = 0.0
    var progressPercentage:Float = 0.0

    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.user = PFUser.currentUser()
        
               //MARK:- Push Notification
        //Sharad Commenting this as we don't want to send any notifications to any one from phone..
        //self.sendNotificationFromApp()
        //self.sendPushToChannels()
        if NSUserDefaults.standardUserDefaults().boolForKey("isUploaded") == false
        {
            self.btnSelectMultipleVideo.alpha = 0
            self.addUIComponents()
            self.progressView.alpha = 0
            self.statusLabel.alpha = 0
            self.btnSelectVideo.alpha = 1
            
        }
        else
        {
            
            getThumbnailFromDir()

            self.lblPercentage.alpha = 1
            self.progressView.alpha = 1
            self.statusLabel.alpha = 1
            self.btnSelectMultipleVideo.alpha = 0
            self.btnSelectVideo.alpha = 0
            let image = UIImage(named:"imgMyPlateMenu")
            self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonItemClicked:"),animated: true)
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGrayColor()
            let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
            dispatch_async(queue, { () -> Void in
                self.addUIComponentsForUploading()
            })
            
        }
    }
    func getThumbnailFromDir()
    {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let getImagePath = paths.stringByAppendingString("/thumbNail.png")
        
        
        let checkValidation = NSFileManager.defaultManager()
        
        if (checkValidation.fileExistsAtPath(getImagePath))
        {
            print("FILE AVAILABLE");
            self.imgviewThumbnail.image = UIImage(data: NSData(contentsOfURL: NSURL(fileURLWithPath:getImagePath))!)
        }


    }
    func saveThumbnailToDir(thumb : UIImage)
    {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let getImagePath = paths.stringByAppendingString("/thumbNail.png")
        
        
        let checkValidation = NSFileManager.defaultManager()
        
        if (checkValidation.fileExistsAtPath(getImagePath))
        {
            print("FILE AVAILABLE");
            do{
                try checkValidation.removeItemAtPath(getImagePath)
                UIImageJPEGRepresentation(thumb, 0.7)?.writeToFile(getImagePath, atomically: true)
                print("FILE NOT AVAILABLE");

            }catch{
                
            }
        }
        else
        {
            UIImageJPEGRepresentation(thumb, 0.7)?.writeToFile(getImagePath, atomically: true)
            print("FILE NOT AVAILABLE");
        }

    }
    func addUIComponentsForUploading()
    {
        
        repeat
        {
            if NSUserDefaults.standardUserDefaults().valueForKey("progressVal") != nil
           {
            self.progressVal = NSUserDefaults.standardUserDefaults().valueForKey("progressVal") as! Float
            if  NSUserDefaults.standardUserDefaults().valueForKey("progressPercentage") != nil
            {
                self.progressPercentage = NSUserDefaults.standardUserDefaults().valueForKey("progressPercentage") as! Float
                print("self.progressVal \(self.progressVal)")
                print("progressPercentage \(progressPercentage)")
            }
           }
            
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                // Fetch Thumbnail Image 

                self.statusLabel.text = "Uploading..."
                self.progressView.progress = self.progressVal
                if self.progressPercentage <= 100
                {
                    self.lblPercentage.text = String(format: "%.2f", self.progressPercentage) + "%"
                }
            })
            
        }
            while (NSUserDefaults.standardUserDefaults().boolForKey("isUploaded") == true)
        // as String//String(format: "%.2f", self.progressPercentage) + "%"
        
        print("DOne")
        if NSUserDefaults.standardUserDefaults().boolForKey("isUploaded") == false
        {
           
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.btnSelectMultipleVideo.alpha = 0
                
                self.addUIComponents()
                self.lblPercentage.alpha = 0
                self.progressView.alpha = 0
                self.statusLabel.alpha = 0
                self.btnSelectVideo.alpha = 1
            })

            
            if NSUserDefaults.standardUserDefaults().valueForKey("errorString") != nil
            {
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "Error", message: NSUserDefaults.standardUserDefaults().valueForKey("errorString") as? String, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                    
                }
                else
                {
                    let alert = UIAlertView(title: "Error", message: NSUserDefaults.standardUserDefaults().valueForKey("errorString") as? String, delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
            else
            {
                if (objc_getClass("UIAlertController") != nil)
                {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "AlzaTV", message: "Video added successfully", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                    
                }
                else
                {
                    let alert = UIAlertView(title: "AlzaTV", message: "Video added successfully", delegate: nil , cancelButtonTitle:"Ok")
                    alert.show()
                }
            }
        }
        NSUserDefaults.standardUserDefaults().removeObjectForKey("progressPercentage")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("progressVal")
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    
    
    func addUIComponents()
    {
        self.btnSelectVideo.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)//UIColor.darkGrayColor()
        self.btnSelectVideo.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnSelectVideo.layer.cornerRadius = 10.0
        self.btnSelectVideo.titleLabel?.font = UIFont(name: "Arial-Bold", size: 15.0)
        self.statusLabel.textColor = UIColor.purpleColor()
        self.statusLabel.textAlignment = NSTextAlignment.Center
        self.statusLabel.layer.cornerRadius = 10.0
        
        
        let image = UIImage(named:"imgMyPlateMenu")
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonItemClicked:"),animated: true)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGrayColor()

    }
    
    func barButtonItemClicked(sender: AnyObject)
    {
        let revealController:SWRevealViewController = self.revealViewController();
        revealController.revealToggle(sender)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBar.hidden = false
        self.navigationController?.navigationItem.hidesBackButton = true
        self.title = "Upload video from device"
    }
    
    /*
    @IBAction func btnSelectMultipleVideoClicked(sender: AnyObject)
    {
        var storyboard:UIStoryboard = self.storyboard!
        var view:UploadVideosViewController = storyboard.instantiateViewControllerWithIdentifier("UploadVideosViewController") as! UploadVideosViewController
        self.navigationController?.pushViewController(view, animated: true)
    }
*/
    
    //MARK:- Select Video Button Action
    @IBAction func btnSelectVideoClicked(sender: AnyObject)
    {
        let picker = UIImagePickerController()
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker.delegate = self
        picker.allowsEditing = false
        picker.mediaTypes = [kUTTypeMovie as String]
        self.presentViewController(picker, animated: true, completion: nil)
    }
    
    //MARK:- ImagePickerController Delegate Methods
   // func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info:NSDictionary!)
    //func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject])
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
//        [picker dismissViewControllerAnimated:YES completion:^{
//            // Edited image works great (if you allowed editing)
//            myUIImageView.image = [info objectForKey:UIImagePickerControllerEditedImage];
//            // AND the original image works great
//            myUIImageView.image = [info objectForKey:UIImagePickerControllerOriginalImage];
//            // AND do whatever you want with it, (NSDictionary *)info is fine now
//            UIImage *myImage = [info objectForKey:UIImagePickerControllerEditedImage];
//            }];
        
      
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isUploaded")
        NSUserDefaults.standardUserDefaults().synchronize()
        self.lblPercentage.alpha = 0
        self.statusLabel.alpha = 1
        self.statusLabel.text = "Connecting..."
        self.btnSelectVideo.alpha = 0
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false
        
        //-----------------------------
        //MARK:- Generate key for image
        let uuid = NSUUID().UUIDString
        print("uuid \(uuid)")
        //-----------------------------
        
        
        print("info ->>> \(info)")
        NSLog("CRashe1")
//        let tempImage =  info[UIImagePickerControllerMediaURL] as! NSURL!
//        NSLog("CRashe2")
//        let pathString = tempImage.relativePath
//        NSLog("CRashe3")
//        print("PathString \(pathString)")
        NSLog("CRashe4")
       
        NSLog("CRashe5")
        //-----------------------------
        //MARK:- Get Video Name picked from Album
        var videoFileName: String!
        let tempImageURL = info[UIImagePickerControllerReferenceURL] as! NSURL!
        let assetLibrary:ALAssetsLibrary = ALAssetsLibrary()
        assetLibrary.assetForURL(tempImageURL, resultBlock: {
            (asset: ALAsset!) in
            if asset != nil {
                let assetRep: ALAssetRepresentation = asset.defaultRepresentation()
             //   let iref = assetRep.fullResolutionImage().takeUnretainedValue()
               // var image =  UIImage(CGImage: iref)
                //print("++++ \(iref)")
                //print("---- \(assetRep)")
                videoFileName = assetRep.filename()
                print("videoFileName ==== \(videoFileName)")
            }
            }, failureBlock: {
                (error: NSError!) in
                
                print("Error!")
            }
        )
        //-----------------------------
        
        //-----------------------------
        //MARK:- Save recorded video to Documents
        //var videoUrl: NSURL = info.objectForKey(UIImagePickerControllerMediaURL) as! NSURL
         NSLog("CRashe6")
      /*  let videoUrl: NSURL = (info as NSDictionary).objectForKey(UIImagePickerControllerReferenceURL) as! NSURL
         NSLog("CRashe7")
        let videoData: NSData = NSData(contentsOfURL: videoUrl)!
          NSLog("CRashe8")
        let paths: NSArray = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
          NSLog("CRashe9")
        let documentsPath: NSString = paths.objectAtIndex(0) as! NSString
          NSLog("CRashe10")
        //-----------------------------
        */      var videoData: NSData!
        var videoURL:NSURL!
        let paths: NSArray = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        NSLog("CRashe9")
        let documentsPath: NSString = paths.objectAtIndex(0) as! NSString
        NSLog("CRashe10")
        if let referenceURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            let fetchResult = PHAsset.fetchAssetsWithALAssetURLs([referenceURL], options: nil)
            if let phAsset = fetchResult.firstObject as? PHAsset {
                PHImageManager.defaultManager().requestAVAssetForVideo(phAsset, options: PHVideoRequestOptions(), resultHandler: { (asset, audioMix, info) -> Void in
                    if let asset = asset as? AVURLAsset
                     {
                        videoData = NSData(contentsOfURL: asset.URL)
                        
                        // optionally, write the video to the temp directory
                        let videoPath = NSTemporaryDirectory() + "tmpMovie.MOV"
                       videoURL = NSURL(fileURLWithPath: videoPath)
                        let writeResult = videoData?.writeToURL(videoURL, atomically: true)
                        
                        if let writeResult = writeResult where writeResult {
                            print("success")
                        }
                        else {
                            print("failure")
                        }
                        let videoNameStr : NSDate = NSDate()
                        let dateFormatter: NSDateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
                        let videoName: String = dateFormatter.stringFromDate(videoNameStr)
                        let str_VideoName : String = "VID_\(videoName)"
                        print("str_VideoName \(str_VideoName)")
                        
                        let docPathVideo: NSString = documentsPath.stringByAppendingPathComponent("/\(str_VideoName).mov")
                        var success: Bool = videoData.writeToFile(docPathVideo as String, atomically: false)
                        print("docPathVideo ---> \(docPathVideo)")
                        //-----------------------------
                        
                        //-----------------------------
                        //MARK:- Generate thumbnail of a video.
                        let asset:AVURLAsset = AVURLAsset(URL: videoURL, options: nil)
                        let gen:AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                        gen.appliesPreferredTrackTransform = true
                        let time:CMTime = CMTimeMakeWithSeconds(0,30)
                        //var error:NSError?
                        var image:CGImageRef!
                        var thumb:UIImage!
                        var imageData : NSData!
                       
                            do
                            {
                                
                                image   =  try gen.copyCGImageAtTime(time, actualTime: nil)//gen.copyCGImageAtTime(time, actualTime:nil, error: &error)
                            }
                            catch
                            {
                                
                            }
                            thumb = UIImage(CGImage: image)
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.imgviewThumbnail.image = thumb as UIImage!
                            })
                            
                            imageData = UIImagePNGRepresentation(thumb)
                            
                        
                        
                        //-----------------------------
                        
                        //-----------------------------
                        //MARK:- Generate and save thumbnail in .png format
//                        let pngPath: String = NSHomeDirectory().stringByAppendingString("Documents/thumbNail.png")//stringByAppendingPathComponent("Documents/thumbNail.png")
//                        UIImagePNGRepresentation(thumb)!.writeToFile(pngPath, atomically: true)
                        self.saveThumbnailToDir(thumb)
                        
                        // Create file manager
                        // var error1: NSError?
                        //   var fileMgr:NSFileManager = NSFileManager.defaultManager()
                        
                        // Point to Document directory
                        //  var docDir  :String = NSHomeDirectory().stringByAppendingString("Documents")//stringByAppendingPathComponent("Documents")
                        
                        // Write out the contents of home directory to console
                        //print("Documents directory: \(fileMgr.contentsOfDirectoryAtPath(docDir))")
                        //-----------------------------
                        
                        
                        
                        //------------ MOV to MP4-------
                        
                        
                        let avAsset = AVURLAsset(URL: NSURL.fileURLWithPath(docPathVideo as String), options: nil)
                        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
                        exportSession!.outputFileType = AVFileTypeMPEG4
                        let docPathVideo1: NSString = documentsPath.stringByAppendingPathComponent("/\(str_VideoName).mp4")
                        exportSession!.outputURL = NSURL.fileURLWithPath(docPathVideo1 as String)
                        
                        exportSession!.exportAsynchronouslyWithCompletionHandler { () -> Void in
                            // NSFileManager.defaultManager().removeItemAtPath(self.filePath(self.mov_extenstion), error: nil)
                            switch exportSession!.status {
                            case AVAssetExportSessionStatus.Completed:
                                print("Completed")
                                
                                //-----------------------------
                                //MARK:- Upload video to Amazon server.
                                let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "us-east-1:631186b9-404c-4341-91f3-230e686b5993")
                                let configuration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: credentialProvider)
                                AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = configuration
                                
                                let uploadFileURL = NSURL.fileURLWithPath(docPathVideo1 as String)
                                
                                let uploadRequest = AWSS3TransferManagerUploadRequest();
                                uploadRequest.bucket = "alzha";
                                uploadRequest.key = uuid + ".mp4";
                                //print("uploadRequest.key \(uploadRequest.key)")
                                uploadRequest.body = uploadFileURL;
                                var totalSize: Double!
                                uploadRequest.uploadProgress = { (bytesSent:Int64, totalBytesSent:Int64,  totalBytesExpectedToSend:Int64) -> Void in
                                    print("totalBytesSent \(totalBytesSent)")
                                    totalSize  = Double(totalBytesSent)/1048576.00
                                    totalSize = round(totalSize)
                                    let progress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
                                    
                                    let progressPercentage = progress * 100
                                    dispatch_sync(dispatch_get_main_queue(), {() -> Void in
                                        self.progressView.alpha = 1
                                        self.statusLabel.alpha = 1
                                        self.lblPercentage.alpha = 1
                                        self.imgviewThumbnail.alpha = 1
                                        self.progressVal =  progress
                                        self.progressView.progress = progress
                                        NSUserDefaults.standardUserDefaults().setValue(self.progressVal, forKey: "progressVal")
                                        NSUserDefaults.standardUserDefaults().synchronize()
                                        self.statusLabel.text = "Uploading..."
                                        if progressPercentage <= 100
                                        {
                                            self.lblPercentage.text = String(format: "%.2f", progressPercentage) + "%"
                                            self.progressPercentage = progressPercentage
                                            NSUserDefaults.standardUserDefaults().setValue( self.progressPercentage, forKey: "progressPercentage")
                                            NSUserDefaults.standardUserDefaults().synchronize()
                                        }
                                        
                                        
                                        //print("totalBytesSent \(totalBytesSent)")
                                    })
                                }
                                
                                let S3TransferManager = AWSS3TransferManager.defaultS3TransferManager()
                                
                                S3TransferManager.upload(uploadRequest).continueWithExecutor(BFExecutor.mainThreadExecutor(), withBlock:{
                                    (task: BFTask!) -> BFTask! in
                                    
                                    if(task.error != nil)
                                    {
                                        
                                        let errorObj: AnyObject = task.error.userInfo
                                        
                                        print("S3 error whille uploading ++++ \(errorObj)")
                                        
                                        self.removeActivityIndicator()
                                        self.view.userInteractionEnabled = true
                                        
                                        if (objc_getClass("UIAlertController") != nil)
                                        {
                                            if #available(iOS 8.0, *) {
                                                let alert = UIAlertController(title: "", message: "Error while uploading video.", preferredStyle: UIAlertControllerStyle.Alert)
                                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                                                self.presentViewController(alert, animated: true, completion: nil)
                                            } else {
                                                // Fallback on earlier versions
                                            }
                                            
                                        }
                                        else
                                        {
                                            let alert = UIAlertView(title: "Error", message:  "Error while uploading video.", delegate: nil , cancelButtonTitle:"Ok")
                                            alert.show()
                                        }
                                    }
                                    else
                                    {
                                        print("upload succesfully")
                                        let strName: String = self.user.objectForKey("name") as! String//self.user.username!
                                        
                                        //-----------------------------
                                        //MARK:- Upload data in class "Media Details"
                                        let imageFile = PFFile(name:"thumbNail.png", data:imageData!)
                                        let mediaDetails = PFObject(className:"MediaDetails")
                                        mediaDetails["residentId"] = UserDefault.getUsersResidentId()
                                        mediaDetails["thumbNail"] = imageFile
                                        mediaDetails["dailyUpload"] = false
                                        mediaDetails["display"] = true
                                        mediaDetails["keyS3"] = uuid + ".mp4"
                                        mediaDetails["username"] = self.user
                                        mediaDetails["name"] = "Video by " + strName
                                        mediaDetails["fileName"] = str_VideoName //videoFileName
                                        //mediaDetails["uploadedOn"] = mediaDetails.createdAt//updatedOnDate
                                        mediaDetails["frequency"] = "Auto"
                                        mediaDetails["size"] = totalSize
                                        mediaDetails["fileUploaded"] = true
                                        
                                        //Ashvini : setting ACL access to videos for both invited and current user
                                        let acl = PFACL()
                                        acl.setPublicWriteAccess(true)
                                        acl.setPublicReadAccess(true)
                                        mediaDetails.ACL = acl
                                        
                                        //MARK: YOGESH
                                        //FIXME NEED TO BE TESTED & duration PUSH TO PARSE June 18, 2015
                                        let pathURL : NSURL = NSURL(fileURLWithPath: docPathVideo as String);
                                        let videoDuration : AVURLAsset = AVURLAsset(URL: pathURL, options: nil);
                                        let duration : CMTime = videoDuration.duration;
                                        let dTotalSeconds : Double = CMTimeGetSeconds(duration)
                                        
                                        mediaDetails["duration"] = dTotalSeconds
                                        //END OF LOGICE June 18, 2015
                                        
                                        
                                        mediaDetails.saveInBackgroundWithBlock {
                                            (success: Bool, error: NSError?) -> Void in
                                            if (success)
                                            {
                                                self.removeActivityIndicator()
                                                self.view.userInteractionEnabled = true
                                                if (objc_getClass("UIAlertController") != nil)
                                                {
                                                    if #available(iOS 8.0, *) {
                                                        let alert = UIAlertController(title: "AlzhaTV", message: "Video Added Successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                                                        self.presentViewController(alert, animated: true, completion: nil)
                                                        
                                                    } else {
                                                        // Fallback on earlier versions
                                                    }
                                                }
                                                else
                                                {
                                                    let alert = UIAlertView(title: "AlzhaTV", message: "Video Added Successfully.", delegate: nil , cancelButtonTitle:"Ok")
                                                    alert.show()
                                                }
                                                self.statusLabel.text = "Uploaded Successfully..."
                                                
                                                //MARK:- Remove Video if exists
                                                if(NSFileManager.defaultManager().fileExistsAtPath(docPathVideo as String))
                                                {
                                                    print("MOV Video removed")
                                                    // NSFileManager.defaultManager().removeItemAtPath(docPathVideo as String, error: nil)
                                                    
                                                    do
                                                    {
                                                        try NSFileManager.defaultManager().removeItemAtPath(docPathVideo as String)
                                                    }
                                                    catch
                                                    {
                                                        
                                                    }
                                                }
                                                
                                                if(NSFileManager.defaultManager().fileExistsAtPath(docPathVideo1 as String))
                                                {
                                                    print("MP4 Video removed")
                                                    // NSFileManager.defaultManager().removeItemAtPath(docPathVideo1 as String, error: nil)
                                                    do
                                                    {
                                                        try NSFileManager.defaultManager().removeItemAtPath(docPathVideo1 as String)
                                                    }
                                                    catch
                                                    {
                                                        
                                                    }
                                                }
                                                //-----------------------------
                                                
                                                let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
                                                let getImagePath = paths.stringByAppendingString("thumbNail.png")
                                                
                                                
                                                let checkValidation = NSFileManager.defaultManager()
                                                
                                                if (checkValidation.fileExistsAtPath(getImagePath))
                                                {
                                                    print("FILE AVAILABLE");
                                                    do{
                                                        try checkValidation.removeItemAtPath(getImagePath)
                                                        
                                                    }catch{
                                                        
                                                    }
                                                }
                                                //-----------------------------
                                                
                                                self.progressView.alpha = 0
                                                self.statusLabel.alpha = 0
                                                self.lblPercentage.alpha = 0
                                                self.imgviewThumbnail.alpha = 0
                                            }
                                            else
                                            {
                                                self.progressView.alpha = 0
                                                self.statusLabel.alpha = 0
                                                self.lblPercentage.alpha = 0
                                                self.imgviewThumbnail.alpha = 0
                                                
                                                // There was a problem, check error.description
                                                self.removeActivityIndicator()
                                                self.view.userInteractionEnabled = true
                                                
                                                //This will get error message sent by Parse SDK.
                                                let errorObj: AnyObject = error!.userInfo
                                                let errorString: String = errorObj.valueForKey("error") as! String
                                                NSUserDefaults.standardUserDefaults().setValue(errorString, forKey: "errorString")
                                                NSUserDefaults.standardUserDefaults().synchronize()
                                                if (objc_getClass("UIAlertController") != nil)
                                                {
                                                    if #available(iOS 8.0, *) {
                                                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                                                        self.presentViewController(alert, animated: true, completion: nil)
                                                    } else {
                                                        // Fallback on earlier versions
                                                    }
                                                    
                                                }
                                                else
                                                {
                                                    let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
                                                    alert.show()
                                                }
                                            }
                                            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isUploaded")
                                            NSUserDefaults.standardUserDefaults().synchronize()
                                        }
                                        //-----------------------------
                                    }
                                    return nil
                                })
                                //-----------------------------
                                //coverter ends here
                                
                                break
                            case AVAssetExportSessionStatus.Failed:
                                print("Failed")
                                break
                                //case AVAssetExportSessionStatus.Cancelled:
                                //    　print("Cancelled")
                            default:
                                break
                            }
                        }
                    }
                })
            }
        }
        /*
        //-----------------------------
        //MARK:- Get date on which thumbnail is uploaded
        var updatedOn : NSDate = NSDate()
        var dateFormatter1: NSDateFormatter = NSDateFormatter()
        dateFormatter1.dateFormat = "MMM dd, yyyy, HHmmss"
        var thumbnailUpdatedOn: String = dateFormatter1.stringFromDate(updatedOn)
        var updatedOnDate: NSDate = dateFormatter1.dateFromString(thumbnailUpdatedOn)!
        print("updatedOnDate \(updatedOnDate)")
        //-----------------------------
        */
        
        //-----------------------------
        //MARK:- Save recorded video name as per current date-time format
       
         self.dismissViewControllerAnimated(true, completion: {})
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.dismissViewControllerAnimated(false, completion: nil)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- Hide status bar
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool)
    {
        UIApplication.sharedApplication().statusBarHidden = true
    }
    
    override func childViewControllerForStatusBarHidden() -> UIViewController?
    {
        return nil
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    //---------------------------------
    
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
    }
    
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }
    
    func sendNotificationFromApp()
    {
        //MARK:- Notification will send from app to Parse.com
        // Create our Installation query
        let pushQuery: PFQuery = PFInstallation.query()!
        pushQuery.whereKey("devieType", equalTo: "ios")
        
        // Send push notification to query
        PFPush.sendPushMessageToQueryInBackground(pushQuery, withMessage: "Notification from app")
    }
    
    func sendPushToChannels()
    {
        let push = PFPush()
        push.setChannel("local")
        push.setMessage("Local channnel notification!")
        push.sendPushInBackground()
    }
    
//    func xyz()
//    {
//         PFPush.sendPushMessageToChannelInBackground("local", withMessage: "Hello Alzha!")
//    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
    override func shouldAutorotate() -> Bool {
        return true
    }
}
