//
//  AppDelegate.swift
//
//  Copyright 2011-present Parse Inc. All rights reserved.
//

import UIKit

import Bolts
import Parse

// If you want to use any of the UI components, uncomment this line
// import ParseUI

// If you want to use Crash Reporting - uncomment this line
// import ParseCrashReporting

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,SWRevealViewControllerDelegate, UIAlertViewDelegate {

    var window: UIWindow?

    //--------------------------------------
    // MARK: - UIApplicationDelegate
    //--------------------------------------

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
              UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.None)
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isUploaded")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("progressPercentage")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("progressVal")
        NSUserDefaults.standardUserDefaults().synchronize()
        // Enable storing and querying data from Local Datastore.
        // Remove this line if you don't want to use Local Datastore features or want to use cachePolicy.
        Parse.enableLocalDatastore()

        // ****************************************************************************
        // Uncomment this line if you want to enable Crash Reporting
        // ParseCrashReporting.enable()
        //
        // Uncomment and fill in with your Parse credentials:
        
     
        // FIXME: PARSE LIVE
         Parse.setApplicationId("cvsQEIDiWfWi4ZrxVom0JoeWLBWyWzZlCaBHoYwO", clientKey: "39RhxeiOwWuTIDaf0w93wqectxD7LvJ0sg5JVDhM") //keys created by Sharad
        
               // FIXME: PARSE TESTING
        // Parse.setApplicationId("Fm9krCzsskSkYmQXD4I3JmuKk8woSkOHuHK4fw50", clientKey: "4iCG7X1unr9A2qpwxWNLtodjrS4yo0mkRgIRrxnV") //KEY Created for development June 2015
        

        PFUser.enableAutomaticUser()
        let defaultACL = PFACL();
        // If you would like all objects to be private by default, remove this line.
        defaultACL.setPublicReadAccess(true)
        PFACL.setDefaultACL(defaultACL, withAccessForCurrentUser:true)
        
        //MARK:- Push Notifications
        // Check to see if this is an iOS 8 device.
        let iOS8 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_7_1)
        if iOS8
        {
            if #available(iOS 8.0, *) {
                let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
                UIApplication.sharedApplication().registerUserNotificationSettings(settings)
                UIApplication.sharedApplication().registerForRemoteNotifications()

            } else {
                // Fallback on earlier versions
            }
          
        }
        else
        {
            // Register for push in iOS 7
            UIApplication.sharedApplication().registerForRemoteNotificationTypes(UIRemoteNotificationType.Badge.union(UIRemoteNotificationType.Sound)  .union(UIRemoteNotificationType.Alert) )
        }
        //
        
        return true
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData)
    {
       
        
        var token: String = deviceToken.description
        token = token.stringByReplacingOccurrencesOfString("<", withString: "", options: NSStringCompareOptions(), range: nil)
        token = token.stringByReplacingOccurrencesOfString(">", withString: "", options: NSStringCompareOptions(), range: nil)
        token = token.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions(), range: nil)
      
        
        /*
        if (objc_getClass("UIAlertController") != nil)
        {
            var alert = UIAlertController(title: "", message: token, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            //self.presentViewController(alert, animated: true, completion: nil)
            self.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)

        }
        else
        {
            var alert = UIAlertView(title: "Error", message: token, delegate: nil , cancelButtonTitle:"Ok")
            alert.show()
        }
        */
        
        // Store the deviceToken in the current Installation and save it to Parse
        let installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        installation.channels = ["local"]
        installation.saveInBackgroundWithBlock { (success, error) -> Void in
      
        }
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError)
    {
    
        
        if (objc_getClass("UIAlertController") != nil)
        {
            if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: "", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                //self.presentViewController(alert, animated: true, completion: nil)
                self.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
           
            
        }
        else
        {
            let alert = UIAlertView(title: "Error", message: error.localizedDescription, delegate: nil , cancelButtonTitle:"Ok")
            alert.show()
        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject])
    {
        PFPush.handlePush(userInfo)
    }
    
    func applicationDidBecomeActive(application: UIApplication)
    {
        let installation = PFInstallation.currentInstallation()
        if installation.badge != 0
        {
            installation.badge = 0
            installation.saveEventually()
        }
    }
    
    @available(iOS 8.0, *)
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings)
    {
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isUploaded")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
        }
