//
//  LoginViewController.swift
//  AlzhaTV
//
//  Created by QMCPL on 31/03/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController, UITextFieldDelegate, UIAlertViewDelegate
{
    @IBOutlet var txtFieldUsername: UITextField!
    @IBOutlet var txtFieldPassword: UITextField!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnRegisterNewUser: UIButton!
    var globalTxtField: UITextField = UITextField()
    var user:PFUser!
    var activityIndicator:UIActivityIndicatorView!
    var userName: String = ""
    var isFirstTime: Bool = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.user = PFUser.currentUser()
        
        
        if(user.username != nil)
        {
            let residentId:String = UserDefault.getUsersResidentId()
            if(residentId.isEmpty || residentId == "")
            {
                let storyboard:UIStoryboard = self.storyboard!
                let view:LovedOneIDViewController = storyboard.instantiateViewControllerWithIdentifier("LovedOneIDViewController") as! LovedOneIDViewController
                self.navigationController?.pushViewController(view, animated: false)
            }
            else
            {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                
                let storyboard:UIStoryboard = self.storyboard!
                
                let menuViewController:MenuViewController = storyboard.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
                
//                //set statusbar to the desired rotation position
//                [[UIApplication sharedApplication] setStatusBarOrientation:UIDeviceOrientationLandscapeLeft animated:NO];
//                
//                //present/dismiss viewcontroller in order to activate rotating.
//                UIViewController *mVC = [[[UIViewController alloc] init] autorelease];
//                [self presentModalViewController:mVC animated:NO];
//                [self dismissModalViewControllerAnimated:NO];
           
                let view:RecordVideoViewController = storyboard.instantiateViewControllerWithIdentifier("RecordVideoViewController") as! RecordVideoViewController
                
                let revealController:SWRevealViewController  = SWRevealViewController()
                revealController.rearViewController = menuViewController
                revealController.frontViewController = UINavigationController(rootViewController: view)
                revealController.delegate = appDelegate;
                
                appDelegate.window!.rootViewController = revealController
                appDelegate.window!.makeKeyAndVisible()
            }
        }
        
        print("getUsersResidentId() -> \(UserDefault.getUsersResidentId())")
        
        self.addUIComponents()
    }
    
    override func viewDidDisappear(animated: Bool)
    {
        
        
    }
   
    @IBAction func actionFrgtPwd(sender: UIButton) {
        
        let frgotView = self.storyboard?.instantiateViewControllerWithIdentifier("forgotPwd") as! ForgotPwdViewController
        self.navigationController?.pushViewController(frgotView, animated: true)
    }
    func addUIComponents()
    {
        self.txtFieldUsername.layer.borderWidth = 0.5
        self.txtFieldUsername.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldUsername.textColor = UIColor.darkGrayColor()
        self.txtFieldUsername.delegate = self
        self.txtFieldUsername.returnKeyType = UIReturnKeyType.Next
        self.txtFieldUsername.keyboardType = UIKeyboardType.EmailAddress
        
        self.txtFieldPassword.layer.borderWidth = 0.5
        self.txtFieldPassword.layer.borderColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0).CGColor
        self.txtFieldPassword.textColor = UIColor.grayColor()
        self.txtFieldPassword.delegate = self
        self.txtFieldPassword.returnKeyType = UIReturnKeyType.Done
        self.txtFieldPassword.secureTextEntry = true
        
        self.btnLogin.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)
        self.btnLogin.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnLogin.titleLabel?.font = UIFont(name: "Arial-Bold", size: 10.0)
        //self.btnLogin.layer.cornerRadius = 10.0
        
        self.btnRegisterNewUser.backgroundColor = UIColor(red: 32/255.0, green: 97/255.0, blue: 245/255.0, alpha: 1.0)
        self.btnRegisterNewUser.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.btnRegisterNewUser.titleLabel?.font = UIFont(name: "Arial-Bold", size: 10.0)
       // self.btnRegisterNewUser.layer.cornerRadius = 10.0
    }
    
    override func viewWillAppear(animated: Bool)
    {
                
        
        self.navigationController?.navigationBar.hidden = true
        self.view.userInteractionEnabled = true
        self.txtFieldUsername.text = ""
        self.txtFieldPassword.text = ""
    }
    
    //MARK: - Email address validations.
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let range = testStr.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
        let result = range != nil ? true : false
        print("Email result \(result)")
        return result
    }
    
    
    func pushRecordView()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let storyboard:UIStoryboard = self.storyboard!
        
        let menuViewController:MenuViewController = storyboard.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
        
        let view:RecordVideoViewController = storyboard.instantiateViewControllerWithIdentifier("RecordVideoViewController") as! RecordVideoViewController
        
        let revealController:SWRevealViewController  = SWRevealViewController()
        revealController.rearViewController = menuViewController
        revealController.frontViewController = UINavigationController(rootViewController: view)
        revealController.delegate = appDelegate;
        
        appDelegate.window!.rootViewController = revealController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    
    //MARK:- Login Button Action
    //Updated logic for login using Email June 21, 2014
    @IBAction func btnLoginClicked(sender: AnyObject)
    {
        self.showActivityIndicator()
        self.view.userInteractionEnabled = false
        
        if !checkForEmptyFields()
        {
            self.removeActivityIndicator()
            self.view.userInteractionEnabled = true
        }
        else
        {
            let query = PFUser.query()
            query!.whereKey("email", equalTo:self.txtFieldUsername.text!.lowercaseString);
            query!.findObjectsInBackgroundWithBlock{
                (objects: [AnyObject]?, error: NSError?) -> Void in
                if error == nil
                {
                    if let objects = objects as? [PFObject]
                    {
                        for object in objects
                        {
                            
                            let userObject : PFObject = object
                            
                            self.userName = userObject["username"] as! String
                        }
                        
                    }
                    
                    if(objects?.count>0)
                    {
                        self.loginUsingEmail();
                        
                    }// Login using only email & no username
                    else
                    {
                        self.showError(error)
                    }
                    
                }
                else
                {
                    self.showError(error)
                }
                
                
            }
            

        }
        
    }
    
    @IBAction func btnForgotPwdClicked(sender: UIButton) {
        PFUser.requestPasswordResetForEmailInBackground("")
    }
    func showError ( error: NSError?)
    {
        self.view.userInteractionEnabled = true
        self.removeActivityIndicator()
        var errorString: String ;
        
        if error == nil
        {
            errorString = "Please enter valid credentials!"
        }
        else{
            //This will get error message sent by Parse SDK.
            let errorObj: AnyObject = error!.userInfo
            errorString = errorObj.valueForKey("error") as! String
        }
        
        if (objc_getClass("UIAlertController") != nil)
        {
            if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
           
        }
        else
        {
            let alert = UIAlertView(title: "Error", message: errorString, delegate: nil , cancelButtonTitle:"Ok")
            alert.show()
        }
    }
    
    func loginUsingEmail()
    {
        
        PFUser.logInWithUsernameInBackground(self.userName, password: self.txtFieldPassword.text!) { (user, error) -> Void in
            if error == nil
            {
                
                
                print("Login User!")
                //This will reset all textfields.
                
                
                let query = PFQuery(className:"Resident")
                query.whereKey("username", equalTo:user!) //Suggested by Sharad 24/04/2015
                query.findObjectsInBackgroundWithBlock{
                    (mainobjects: [AnyObject]?, error: NSError?) -> Void in
                    if error == nil
                    {
                        
                        // The find succeeded.
                 print("Successfully retrieved \(mainobjects!.count) scores.")
                        if (mainobjects!.count != 0)
                        {
                            var residentObject : PFObject!
                            if let obj = mainobjects as? [PFObject]
                            {
                                residentObject = obj[0] as PFObject
                            }
                            let residentid: String = residentObject["residentId"] as! String
                            
                            UserDefault.setUsersResidentId(residentid)
                            NSUserDefaults.standardUserDefaults().synchronize()
                            self.view.userInteractionEnabled = true
                            self.removeActivityIndicator()
                            self.pushRecordView()
                        }
                        else
                        {
                            self.view.userInteractionEnabled = true
                            self.removeActivityIndicator()
                            
                            let storyboard:UIStoryboard = self.storyboard!
                            let view:LovedOneIDViewController = storyboard.instantiateViewControllerWithIdentifier("LovedOneIDViewController") as! LovedOneIDViewController
                            self.navigationController?.pushViewController(view, animated: true)
                        }
                    }
                    else
                    {
                    self.showError(error)
                    }
                }
                
                
                
            }
            else
            {
                self.showError(error)
            }
        }
        
    }
    
    //MARK:- Empty fields validation
    func checkForEmptyFields() -> Bool
    {
        if(self.txtFieldUsername.text == "" || self.txtFieldUsername.text!.isEmpty || self.txtFieldPassword.text == "" || self.txtFieldPassword.text!.isEmpty )
        {
            self.view.userInteractionEnabled = true
            self.removeActivityIndicator()
            
            if (objc_getClass("UIAlertController") != nil)
            {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Alert", message: "Username or Password field can not be blank.", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
               
            }
            else
            {
                let alert = UIAlertView(title: "Alert", message: "Username or Password field can not be blank.", delegate: nil , cancelButtonTitle:"Ok")
                alert.show()
            }
            return false
        }
        else if (!self.isValidEmail(self.txtFieldUsername.text!))
        {
            if (objc_getClass("UIAlertController") != nil)
            {
               if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "Alert", message: "Please enter valid email address!", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
                
            }
            else
            {
                let alert = UIAlertView(title: "Alert", message: "Please enter valid email address!", delegate: nil , cancelButtonTitle:"Ok")
                alert.show()
            }
            
          return false
        }
        else
        {
            return true
        }
    }
    
    //MARK:- Register Button Action
    @IBAction func btnRegisterNewUserClicked(sender: AnyObject)
    {
        let storyboard:UIStoryboard = self.storyboard!
        let view:RegisterViewController = storyboard.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    //MARK:- UITextFieldDelegate Methods
    func textFieldDidBeginEditing(textField: UITextField)
    {
     
        self.animateTextField(textField, up: true)
        self.globalTxtField = textField
        return
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        self.animateTextField(textField, up: false)
        self.globalTxtField = textField
        return
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == self.txtFieldUsername
        {
            self.txtFieldPassword.becomeFirstResponder()
        }
        else
        {
            self.btnLoginClicked("")
            textField.resignFirstResponder()
        }
        return true
    }
    
    func showActivityIndicator()
    {
        if (self.activityIndicator == nil)
        {
            self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            self.activityIndicator.frame = CGRectMake(0, 0, 40, 40);
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 150)
    }
    func removeActivityIndicator()
    {
        if(self.activityIndicator != nil)
        {
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*- (void) animateTextField: (UITextField*) textField up: (BOOL) up
    {
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    }*/
    
    func animateTextField(textField: UITextField, up: Bool)
    {
        
        print("CGRectOffset \(self) ")
        let movementDistance = 80
        let movementDuration = 0.3
        
        let movement:Int  = up ? -movementDistance : movementDistance
        
        UIView.beginAnimations("anim", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = CGRectOffset(self.view.frame, 0,CGFloat(movement))
        UIView.commitAnimations()
        
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }

    
}
